﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace WoolbrightCMSData.Classes
{
    public class ValidateFileAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
            {
                return false;
            }

            if (file.ContentLength > 3 * 1024 * 1024) //5 MB
            {
                return false;
            }

            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    if (
                        img.RawFormat.Equals(ImageFormat.Png)
                        || img.RawFormat.Equals(ImageFormat.Gif)
                        || img.RawFormat.Equals(ImageFormat.Jpeg)
                        || img.RawFormat.Equals(ImageFormat.Bmp)
                  )
                    {
                        return true;
                    }
                    //return img.RawFormat.Equals(ImageFormat.Png);
                }
            }
            catch { }
            return false;
        }
    }
}