﻿using System;

namespace Woolbright.Web.Data.Classes
{
    public static class Enums
    {
        public enum ContentTypes
        {
            Image,
            Video,
            Document
        }

        public enum BayTypes
        {
            Single,
            Double,
            Triple
        }
        
        public enum FreeFormValueType
        {
            String,
            Number,
            Location,
            Content,
            Tag
        }

        [Flags] // Must start at '1' not '0' for Flags-Enums
        public enum SiteVisibilityFlags : short
        {
            None = 1,
            Corporate = 2,
            Properties = 4,
            Kiosk = 8
        }

        public enum OccupancyState
        {
            Occupied,
            Available
        }
      

    }
}