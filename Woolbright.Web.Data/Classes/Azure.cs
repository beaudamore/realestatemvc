﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Web.Hosting;
using System.Web.Mvc;
using Microsoft.WindowsAzure;
using Woolbright.Web.Data.DAL;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.Classes
{
    public class Azure : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IResearchArticleRepository _researchArticleRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly IContentRepository _contentRepository;

        public Azure(
            IPropertyRepository propertyRepository,
            IResearchArticleRepository researchArticleRepository,
            IUserProfileRepository userRepository,
            IContentRepository contentRepository
               )
        {
            _propertyRepository = propertyRepository;
            _researchArticleRepository = researchArticleRepository;
            _userRepository = userRepository;
            _contentRepository = contentRepository;
        }

        public static string PostBlobsToAzure(string containerName, List<string> filePaths, byte[] thumbBytes, DateTime lastModifiedDateForIndexHtml)
        {
            // get Azure container
            var container = GetAzureContainerByName(containerName);
            
            // Create the container if it doesn't already exist.
            container.CreateIfNotExists();

            // set perms
            container.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess =
                        BlobContainerPublicAccessType.Blob
                });

            CloudBlockBlob blockBlobThumbBytes = container.GetBlockBlobReference("thumbnail.jpg");
            using (var ms = new MemoryStream(thumbBytes, false))
            {
                blockBlobThumbBytes.UploadFromStream(ms);
            }

            // create key,value pair for metadata uploadDate for comparing index.html later
            var lastModifiedDateForIndexHtmlPair = new KeyValuePair<string, string>("uploadDate", lastModifiedDateForIndexHtml.ToString(CultureInfo.InvariantCulture));

            // Create or overwrite the "myblob" blob with contents from a local file.
            foreach (var path in filePaths)
            {
                // extract just the file name
                var fileName = path.Substring(path.LastIndexOf("\\", StringComparison.Ordinal) + 1,
                    path.Length - path.LastIndexOf("\\", StringComparison.Ordinal) - 1)
                    .ToLower();
                // Retrieve reference.
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                // if it's the 'index.html', insert uploadDate metadata for comparison
                if (fileName.ToLower().Contains("index.html"))
                {
                    blockBlob.Metadata.Add(lastModifiedDateForIndexHtmlPair);
                }
                // upload file
                using (var fileStream = System.IO.File.OpenRead(path))
                {
                    blockBlob.UploadFromStream(fileStream);
                }
            }
            return blockBlobThumbBytes.StorageUri.PrimaryUri.ToString();
        }

        public static void GetIndexHtmlFromAzureIfNewerByContainerName(ResearchArticle researchArticle)
        {
            // get Azure container
            var container = GetAzureContainerByName(researchArticle.BodyAzureContainerName);
          
            // Retrieve reference to a blob named "index.html".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference("index.html");
            blockBlob.FetchAttributes();

            // get local index.html
            var localIndexHtml = new FileInfo(HostingEnvironment.MapPath(ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] + researchArticle.BodyRelativeLocalPath + "/index.html"));
            // is it newer?
            string azureIndexHtmlUploadDateString;
            blockBlob.Metadata.TryGetValue("uploadDate", out azureIndexHtmlUploadDateString);
            if (Convert.ToDateTime(azureIndexHtmlUploadDateString) > localIndexHtml.LastWriteTime || !localIndexHtml.Exists)
                {
                // Save blob contents to a file.
                var savePath =
                    HostingEnvironment.MapPath(
                        ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] +
                        researchArticle.BodyRelativeLocalPath + '\\' + "index.html");
                using (var fileStream = System.IO.File.OpenWrite(savePath))
                {
                    blockBlob.DownloadToStream(fileStream);
                }
            }
        }

        public static void DeleteContainer(string containerName)
        {
            // get Azure container
            var container = GetAzureContainerByName(containerName);
            container.Delete();
        }
        
        public static void CreateFolderAndSetPermissionsIfNotExists(string directoryToCreate)
        {
            // if null throw easily trackable exception
            if (directoryToCreate == null) throw new ArgumentNullException(@"[directoryToCreate is null]");
            if (System.IO.Directory.Exists(HostingEnvironment.MapPath(directoryToCreate))) return;

            System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath(directoryToCreate));
            var info = new DirectoryInfo(HostingEnvironment.MapPath(directoryToCreate));
            var security = info.GetAccessControl();
            var appPoolName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            security.AddAccessRule(new FileSystemAccessRule(appPoolName, FileSystemRights.Modify, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
            security.AddAccessRule(new FileSystemAccessRule(appPoolName, FileSystemRights.Modify, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            info.SetAccessControl(security);
        }

        public static CloudBlobContainer GetAzureContainerByName(string containerName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            return container;
        }

        // end of class
    }
}