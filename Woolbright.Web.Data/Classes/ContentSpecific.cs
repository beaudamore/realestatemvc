﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.Classes
{
    public class ContentSpecific
    {
        //private static IResearchArticleRepository _researchArticleRepository;
        //public ContentSpecific(
        //    IResearchArticleRepository researchArticleRepository
        //    )
        //{
        //    _researchArticleRepository = researchArticleRepository;
        //}       
        
        public static List<Content> RequestFilesToContentList(HttpFileCollectionBase files, out string errorMessage)
        {
            var contentList = new List<Content>();
            var imageErrorMessage = string.Empty;
            foreach (string fileName in files)
            {
                var postedFile = files[fileName];
                // check if blank file
                if (postedFile != null && postedFile.ContentLength > 0)
                {
                    // ----------- validate if image -----------
                    var tempContent = ImageSpecific.ValidateImageContent(postedFile, out imageErrorMessage);
                    // it is an image?
                    if (string.IsNullOrEmpty(imageErrorMessage) && tempContent.ContentBytes != null)
                    {
                        tempContent.ContentType = Enums.ContentTypes.Image;
                    }
                    else // it isn't an image, therefore must be document...(or a video/embed?)
                    {
                        tempContent.ContentType = Enums.ContentTypes.Document;
                    }
                    tempContent.FileName = postedFile.FileName;
                    tempContent.MimeType = postedFile.ContentType;
                    contentList.Add(tempContent);
                }
            }
            errorMessage = imageErrorMessage;
            return contentList;
        }

        public static string GetCommaDelimNameStringFromTags(List<Tag> tagList)
        {
            var returnString = string.Empty;
            foreach (var tag in tagList)
            {
                if (returnString.Length > 0)
                    returnString += ", ";
                returnString += tag.TagName;
            }
            return returnString;
        }

        public static string SlugifyResearchArticleLinkText(ResearchArticle article)
        {
            // create a nice SEO friendly link to the article...
            var returnString = article.Title.Replace(" ", "-") + "-on-" +
                                  article.DatePublished.ToShortDateString().Replace("/", "-");
            return returnString;
        }
        public static string SlugifyPropertyDetailsLinkText(Property property)
        {
            // create a nice SEO friendly link to the article...
            var returnString = property.Name.Replace(" ", "-") + "-in-" +
                                  property.City.Replace(" ", "-");
            return returnString;
        }

        public static DbGeography FindCoordinatesFromGoogleMaps(string address)
        {
            //                                                              Long        Lat
            // example: property.Location = DbGeography.FromText("POINT(-122.335197 47.646711");
            DbGeography g = null;
            var url = "http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false";
            var request = WebRequest.Create(url);
            using (WebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    var dsResult = new DataSet();
                    dsResult.ReadXml(reader);
                    foreach (var location in from DataRow row in dsResult.Tables["result"].Rows
                                             select dsResult.Tables["geometry"]
                                                 .Select("result_id = " + row["result_id"])[0]["geometry_id"].ToString() into geometryId
                                             select dsResult.Tables["location"].Select("geometry_id = " + geometryId)[0])
                    {
                        g = DbGeography.FromText("POINT(" + location["lng"] + " " + location["lat"] + ")");
                    }
                    // ---RESHARPER modified the below to be the above ^ ---
                    //foreach (DataRow row in dsResult.Tables["result"].Rows)
                    //{
                    //    var geometryId = dsResult.Tables["geometry"]
                    //        .Select("result_id = " + row["result_id"])[0]["geometry_id"].ToString();
                    //    var location = dsResult.Tables["location"].Select("geometry_id = " + geometryId)[0];
                    //    g = DbGeography.FromText("POINT(" + location["lng"] + " " + location["lat"] + ")");
                    //}
                    return g;
                }
            }
        }
    }
}