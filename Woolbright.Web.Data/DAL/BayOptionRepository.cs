﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class BayOptionRepository : IBayOptionRepository
    {
        private readonly CMSContext _context;

        public BayOptionRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<BayOption> GetBayOptions()
        {
            return _context.BayOptions.ToList();
        }

        public BayOption GetBayOptionById(Guid id)
        {
            return _context.BayOptions.Find(id);
        }

        public void InsertBayOption(BayOption bayOption)
        {
            _context.BayOptions.Add(bayOption);
        }

        public void DeleteBayOption(Guid bayOptionId)
        {
            var bayOption = _context.BayOptions.Find(bayOptionId);
            _context.BayOptions.Remove(bayOption);
        }

        public void UpdateBayOption(BayOption bayOption)
        {
            _context.Entry(bayOption).State = EntityState.Modified;
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                throw;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
