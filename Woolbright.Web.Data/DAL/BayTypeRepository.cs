﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class BayTypeRepository : IBayTypeRepository
    {
        private readonly CMSContext _context;

        public BayTypeRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<BayType> GetBayTypes()
        {
            return _context.BayTypes.ToList();
        }

        public BayType GetBayTypeByGuid(Guid id)
        {
            return _context.BayTypes.Find(id);
        }

        public void InsertBayType(BayType bayType)
        {
            _context.BayTypes.Add(bayType);
        }

        public void DeleteBayType(Guid bayOptionId)
        {
            var bayOption = _context.BayOptions.Find(bayOptionId);
            _context.BayOptions.Remove(bayOption);
        }

        public void UpdateBayType(BayType bayType)
        {
            _context.Entry(bayType).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
