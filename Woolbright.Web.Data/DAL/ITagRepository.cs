﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface ITagRepository : IDisposable
    {
        IEnumerable<Tag> GetTags();
        Tag GetTagById(Guid tagId);
        void InsertTag(Tag tag);
        void UpdateTag(Tag tag);
        void DeleteTag(Guid tagId);
        void Save();
    }
}