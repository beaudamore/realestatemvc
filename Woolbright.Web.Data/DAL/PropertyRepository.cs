﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly CMSContext _context;

        public PropertyRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<Property> GetProperties()
        {
            return _context.Properties.ToList();
        }

        public Property GetPropertyById(int id)
        {
            return _context.Properties.Find(id);
        }

        public void InsertProperty(Property property)
        {
            _context.Properties.Add(property);
        }

        public void DeletePropertyById(int propertyId)
        {
            var property = _context.Properties.Find(propertyId);
            _context.Properties.Remove(property);
        }

        public void UpdateProperty(Property property)
        {
            _context.Entry(property).State = EntityState.Modified;
        }

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                throw;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
