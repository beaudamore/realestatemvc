﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class FreeFormTemplateRepository : IFreeFormTemplateRepository
    {
        private readonly CMSContext _context;

        public FreeFormTemplateRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<FreeFormTemplate> GetFreeFormTemplates()
        {
            return _context.FreeFormTemplates.ToList();
        }

        public FreeFormTemplate GetFreeFormTemplateById(Guid id)
        {
            return _context.FreeFormTemplates.Find(id);
        }

        public void InsertFreeFormTemplate(FreeFormTemplate freeFormTemplate)
        {
            _context.FreeFormTemplates.Add(freeFormTemplate);
        }

        public void DeleteFreeFormTemplate(Guid freeFormTemplateId)
        {
            FreeFormTemplate freeFormTemplate = _context.FreeFormTemplates.Find(freeFormTemplateId);
            _context.FreeFormTemplates.Remove(freeFormTemplate);
        }

        public void UpdateFreeFormTemplate(FreeFormTemplate freeFormTemplate)
        {
            _context.Entry(freeFormTemplate).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
