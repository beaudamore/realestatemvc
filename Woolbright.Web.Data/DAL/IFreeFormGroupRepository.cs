﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IFreeFormGroupRepository : IDisposable
    {
        IEnumerable<FreeFormGroup> GetFreeFormGroups();
        FreeFormGroup GetFreeFormGroupById(int freeFormGroupId);
        void InsertFreeFormGroup(FreeFormGroup freeFormGroup);
        void UpdateFreeFormGroup(FreeFormGroup freeFormGroup);
        void DeleteFreeFormGroup(int freeFormGroupId);
        void Save();
    }
}