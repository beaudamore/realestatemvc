﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class ExecutiveSectionRepository : IExecutiveSectionRepository
    {
        private readonly CMSContext _context;

        public ExecutiveSectionRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<ExecutiveSection> GetExecutiveSections()
        {
            return _context.ExecutiveSections.ToList();
        }

        public ExecutiveSection GetExecutiveSectionById(int id)
        {
            return _context.ExecutiveSections.Find(id);
        }

        public void InsertExecutiveSection(ExecutiveSection executiveSection)
        {
            _context.ExecutiveSections.Add(executiveSection);
        }

        public void DeleteExecutiveSection(int executiveSectionId)
        {
            ExecutiveSection executiveSection = _context.ExecutiveSections.Find(executiveSectionId);
            _context.ExecutiveSections.Remove(executiveSection);
        }

        public void UpdateExecutiveSection(ExecutiveSection executiveSection)
        {
            _context.Entry(executiveSection).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
