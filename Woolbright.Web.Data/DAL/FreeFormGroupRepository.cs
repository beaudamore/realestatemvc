﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class FreeFormGroupRepository : IFreeFormGroupRepository
    {
        private readonly CMSContext _context;

        public FreeFormGroupRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<FreeFormGroup> GetFreeFormGroups()
        {
            return _context.FreeFormGroups.ToList();
        }

        public FreeFormGroup GetFreeFormGroupById(int id)
        {
            return _context.FreeFormGroups.Find(id);
        }

        public void InsertFreeFormGroup(FreeFormGroup freeFormGroup)
        {
            _context.FreeFormGroups.Add(freeFormGroup);
        }

        public void DeleteFreeFormGroup(int freeFormGroupId)
        {
            FreeFormGroup freeFormGroup = _context.FreeFormGroups.Find(freeFormGroupId);
            _context.FreeFormGroups.Remove(freeFormGroup);
        }

        public void UpdateFreeFormGroup(FreeFormGroup freeFormGroup)
        {
            _context.Entry(freeFormGroup).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
