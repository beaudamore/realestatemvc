﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IPropertyRepository : IDisposable
    {
        IEnumerable<Property> GetProperties();
        Property GetPropertyById(int propertyId);
        void UpdateProperty(Property property);
        void InsertProperty(Property property);
        void DeletePropertyById(int propertyId);
        void Save();
    }
}