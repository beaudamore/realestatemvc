﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IContentRepository : IDisposable
    {
        IEnumerable<Content> GetContent();
        Content GetContentById(Guid contentId);
        void UpdateContent(Content content);
        void DeleteContent(Guid contentId);
        void Save();
    }
}