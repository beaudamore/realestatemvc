﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IExecutiveRepository : IDisposable
    {
        IEnumerable<Executive> GetExecutives();
        Executive GetExecutiveById(int executiveId);
        void InsertExecutive(Executive executive);
        void UpdateExecutive(Executive executive);
        void DeleteExecutive(int executiveId);
        void Save();
    }
}