﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class CMSContext : DbContext
    {
        public CMSContext()
            : base("CMSContext")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Executive> Executives { get; set; }
        public DbSet<ExecutiveSection> ExecutiveSections { get; set; }
        public DbSet<ExecutiveSectionMapping> ExecutiveSectionMappings { get; set; }

        public DbSet<Market> Markets { get; set; }

        public DbSet<Property> Properties { get; set; }

        public DbSet<BayOption> BayOptions { get; set; }

        public DbSet<BayType> BayTypes { get; set; }
     
        public DbSet<ResearchArticle> ResearchArticles { get; set; }

        public DbSet<Content> Content { get; set; }

        // freeformfields
        public DbSet<FreeFormTemplate> FreeFormTemplates { get; set; }
        public DbSet<FreeFormGroup> FreeFormGroups { get; set; }
        public DbSet<FreeFormField> FreeFormFields { get; set; }

        // tags
        public DbSet<Tag> Tags { get; set; }

        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Property>()
                .HasOptional(c => c.CorporateContact)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Property>()
               .HasOptional(c => c.LeasingAgent)
               .WithMany()
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Executive>()
                .HasRequired(c => c.ProfilePictureContent)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Executive>()
                .HasRequired(c => c.ProfileSidePictureContent)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Executive>()
                .HasRequired(x => x.UserProfile)
                .WithOptional(s => s.Executive)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tag>()
                .HasOptional(x => x.ResearchArticle);
            modelBuilder.Entity<Tag>()
                .HasOptional(x => x.Property);

                
        }
     
    }
}