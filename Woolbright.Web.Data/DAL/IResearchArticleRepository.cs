﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IResearchArticleRepository : IDisposable
    {
        IEnumerable<ResearchArticle> GetResearchArticles();
        ResearchArticle GetResearchArticleById(int researchArticleId);
        void UpdateResearchArticle(ResearchArticle researchArticle);
        void InsertResearchArticle(ResearchArticle researchArticle);
        void DeleteResearchArticle(int researchArticle);
        void Save();
    }
}