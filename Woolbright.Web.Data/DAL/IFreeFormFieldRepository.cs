﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IFreeFormFieldRepository : IDisposable
    {
        IEnumerable<FreeFormField> GetFreeFormFields();
        FreeFormField GetFreeFormFieldById(Guid freeFormFieldId);
        void InsertFreeFormField(FreeFormField freeFormField);
        void UpdateFreeFormField(FreeFormField freeFormField);
        void DeleteFreeFormField(Guid freeFormFieldId);
        void Save();
    }
}