﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class ExecutiveSectionMappingRepository : IExecutiveSectionMappingRepository
    {
        private readonly CMSContext _context;

        public ExecutiveSectionMappingRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<ExecutiveSectionMapping> GetExecutiveSectionMappings()
        {
            return _context.ExecutiveSectionMappings.ToList();
        }

        public ExecutiveSectionMapping GetExecutiveSectionMappingById(int id)
        {
            return _context.ExecutiveSectionMappings.Find(id);
        }
        //public List<ExecutiveSectionMapping> GetExecutiveSectionMappingsBySectionIdAndExecutiveId(int SectionId, int ExecutiveId)
        //{
        //   var mappings = context.ExecutiveSectionMappings
        //      .Where(x => x.ExecutiveSectionId == SectionId && x.ExecutiveId != ExecutiveId)
        //      .ToList();                  
        //    return (mappings.ToList());
        //}

        public void InsertExecutiveSectionMapping(ExecutiveSectionMapping executiveSectionMapping)
        {
            _context.ExecutiveSectionMappings.Add(executiveSectionMapping);
        }

        public void DeleteExecutiveSectionMapping(int executiveSectionMappingId)
        {
            var executiveSectionMapping = _context.ExecutiveSectionMappings.Find(executiveSectionMappingId);
            _context.ExecutiveSectionMappings.Remove(executiveSectionMapping);
        }

        public void DeleteExecutiveSectionMappingRange(ICollection<ExecutiveSectionMapping> executiveSectionMappings)
        {
            _context.ExecutiveSectionMappings.RemoveRange(executiveSectionMappings);
          
        }

        public void UpdateExecutiveSectionMapping(ExecutiveSectionMapping executiveSectionMapping)
        {
            _context.Entry(executiveSectionMapping).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
