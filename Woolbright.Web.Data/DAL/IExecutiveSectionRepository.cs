﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IExecutiveSectionRepository : IDisposable
    {
        IEnumerable<ExecutiveSection> GetExecutiveSections();
        ExecutiveSection GetExecutiveSectionById(int executiveSectionId);
        void InsertExecutiveSection(ExecutiveSection executiveSection);
        void UpdateExecutiveSection(ExecutiveSection executiveSection);
        void DeleteExecutiveSection(int executiveSectionId);
        void Save();
    }
}