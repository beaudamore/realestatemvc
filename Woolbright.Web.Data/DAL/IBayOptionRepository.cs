﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IBayOptionRepository : IDisposable
    {
        IEnumerable<BayOption> GetBayOptions();
        BayOption GetBayOptionById(Guid bayOptionId);
        void InsertBayOption(BayOption bayOption);
        void UpdateBayOption(BayOption bayOption);
        void DeleteBayOption(Guid bayOptionId);
        void Save();
    }
}