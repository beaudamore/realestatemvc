﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly CMSContext _context;

        public UserProfileRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<UserProfile> GetUserProfiles()
        {
            return _context.UserProfiles.ToList();
        }

        public UserProfile GetUserProfileById(int userId)
        {
            return _context.UserProfiles.Find(userId);
        }
        public UserProfile GetUserProfileByFullName(string fullName)
        {
            var name = fullName.Split(' ');
            var first = name[0];
            var last = name[1];
            return _context.UserProfiles.FirstOrDefault(x => x.FirstName == first && x.LastName == last);
        }

        public UserProfile GetUserProfileByGuid(Guid userGuid)
        {
            return _context.UserProfiles
                .FirstOrDefault(x => x.UserProfileGuid == userGuid);
        }

        public void InsertUserProfile(UserProfile user)
        {
            _context.UserProfiles.Add(user);
        }

        public void DeleteUserProfile(int userId)
        {
            UserProfile user = _context.UserProfiles.Find(userId);
            _context.UserProfiles.Remove(user);
        }

        public void UpdateUserProfile(UserProfile user)
        {
            _context.Entry(user).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
