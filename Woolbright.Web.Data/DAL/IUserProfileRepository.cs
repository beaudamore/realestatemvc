﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IUserProfileRepository : IDisposable
    {
        IEnumerable<UserProfile> GetUserProfiles();
        UserProfile GetUserProfileById(int userId);
        UserProfile GetUserProfileByFullName(string fullName);
        UserProfile GetUserProfileByGuid(Guid userGuid);
        void InsertUserProfile(UserProfile user);
        void UpdateUserProfile(UserProfile user);
        void DeleteUserProfile(int userId);
        void Save();
    }
}