﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class MarketRepository : IMarketRepository
    {
        private readonly CMSContext _context;

        public MarketRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<Market> GetMarkets()
        {
            return _context.Markets.ToList();
        }

        public Market GetMarketById(int id)
        {
            return _context.Markets.Find(id);
        }

        public void InsertMarket(Market market)
        {
            _context.Markets.Add(market);
        }

        public void DeleteMarket(int marketId)
        {
            Market market = _context.Markets.Find(marketId);
            _context.Markets.Remove(market);
        }

        public void UpdateMarket(Market market)
        {
            _context.Entry(market).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
