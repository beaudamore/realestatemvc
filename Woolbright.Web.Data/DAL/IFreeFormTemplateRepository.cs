﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IFreeFormTemplateRepository : IDisposable
    {
        IEnumerable<FreeFormTemplate> GetFreeFormTemplates();
        FreeFormTemplate GetFreeFormTemplateById(Guid freeFormTemplateId);
        void InsertFreeFormTemplate(FreeFormTemplate freeFormTemplate);
        void UpdateFreeFormTemplate(FreeFormTemplate freeFormTemplate);
        void DeleteFreeFormTemplate(Guid freeFormTemplateId);
        void Save();
    }
}