﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class ResearchArticleRepository : IResearchArticleRepository
    {
        private readonly CMSContext _context;

        public ResearchArticleRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<ResearchArticle> GetResearchArticles()
        {
            return _context.ResearchArticles.ToList();
        }

        public ResearchArticle GetResearchArticleById(int id)
        {
            return _context.ResearchArticles.Find(id);
        }

        public void InsertResearchArticle(ResearchArticle researchArticle)
        {
            _context.ResearchArticles.Add(researchArticle);
        }

        public void DeleteResearchArticle(int researchArticle)
        {
            ResearchArticle ResearchArticle = _context.ResearchArticles.Find(researchArticle);
            _context.ResearchArticles.Remove(ResearchArticle);
        }

        public void UpdateResearchArticle(ResearchArticle researchArticle)
        {
            _context.Entry(researchArticle).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
