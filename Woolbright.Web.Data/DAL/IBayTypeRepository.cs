﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IBayTypeRepository : IDisposable
    {
        IEnumerable<BayType> GetBayTypes();
        BayType GetBayTypeByGuid(Guid bayTypeGuid);
        void InsertBayType(BayType bayType);
        void UpdateBayType(BayType bayType);
        void DeleteBayType(Guid bayTypeGuid);
        void Save();
    }
}