﻿using System;
using System.Collections.Generic;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class TagRepository : ITagRepository
    {
        private readonly CMSContext _context;

        public TagRepository(CMSContext context)
        {
            this._context = context;
        }

        public IEnumerable<Tag> GetTags()
        {
            return _context.Tags.ToList();
        }

        public Tag GetTagById(Guid id)
        {
            return _context.Tags.Find(id);
        }

        public void InsertTag(Tag tag)
        {
            _context.Tags.Add(tag);
        }

        public void DeleteTag(Guid tagId)
        {
            Tag tag = _context.Tags.Find(tagId);
            _context.Tags.Remove(tag);
        }

        public void UpdateTag(Tag tag)
        {
            _context.Entry(tag).State = System.Data.Entity.EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
