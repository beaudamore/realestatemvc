﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IMarketRepository : IDisposable
    {
        IEnumerable<Market> GetMarkets();
        Market GetMarketById(int marketId);
        void UpdateMarket(Market market);
        void InsertMarket(Market market);
        void DeleteMarket(int market);
        void Save();
    }
}