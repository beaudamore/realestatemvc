﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class ExecutiveRepository : IExecutiveRepository
    {
        private readonly CMSContext _context;

        public ExecutiveRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<Executive> GetExecutives()
        {
            var executives = _context.Executives;
            var test = new List<Executive>();
            if (executives.Any())
            {
                test = executives.ToList();
                // sorting
                test.ForEach(s => s.ExecutiveSectionMappings = s.ExecutiveSectionMappings
                                                                          .OrderBy(x => x.ExecutiveSectionId)
                                                                          .ToList());
            }
            return test;
        }

        public Executive GetExecutiveById(int id)
        {
            return _context.Executives.Find(id);
        }

        public void InsertExecutive(Executive executive)
        {
            _context.Executives.Add(executive);
        }

        public void DeleteExecutive(int executiveId)
        {
            var executive = _context.Executives.Find(executiveId);
            _context.Executives.Remove(executive);
        }

        public void UpdateExecutive(Executive executive)
        {
            _context.Entry(executive).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
