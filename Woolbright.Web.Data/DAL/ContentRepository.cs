﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class ContentRepository : IContentRepository
    {
        private readonly CMSContext _context;

        public ContentRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<Content> GetContent()
        {
            return _context.Content.ToList();
        }

        public Content GetContentById(Guid id)
        {
            return _context.Content.Find(id);
        }

        public void InsertContent(Content content)
        {
            _context.Content.Add(content);
        }

        public void DeleteContent(Guid contentId)
        {
            var content = _context.Content.Find(contentId);
            _context.Content.Remove(content);
        }

        public void UpdateContent(Content content)
        {
            _context.Entry(content).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
