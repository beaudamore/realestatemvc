﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public class FreeFormFieldRepository : IFreeFormFieldRepository
    {
        private readonly CMSContext _context;

        public FreeFormFieldRepository(CMSContext context)
        {
            _context = context;
        }

        public IEnumerable<FreeFormField> GetFreeFormFields()
        {
            return _context.FreeFormFields.ToList();
        }

        public FreeFormField GetFreeFormFieldById(Guid id)
        {
            return _context.FreeFormFields.Find(id);
        }

        public void InsertFreeFormField(FreeFormField freeFormField)
        {
            _context.FreeFormFields.Add(freeFormField);
        }

        public void DeleteFreeFormField(Guid freeFormFieldId)
        {
            FreeFormField freeFormField = _context.FreeFormFields.Find(freeFormFieldId);
            _context.FreeFormFields.Remove(freeFormField);
        }

        public void UpdateFreeFormField(FreeFormField freeFormField)
        {
            _context.Entry(freeFormField).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}
