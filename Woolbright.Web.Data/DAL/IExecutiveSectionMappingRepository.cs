﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Data.DAL
{
    public interface IExecutiveSectionMappingRepository : IDisposable
    {
        IEnumerable<ExecutiveSectionMapping> GetExecutiveSectionMappings();
        ExecutiveSectionMapping GetExecutiveSectionMappingById(int executiveSectionMappingId);
        //List<ExecutiveSectionMapping> GetExecutiveSectionMappingsBySectionIdAndExecutiveId(int SectionId, int ExecutiveId);
        void InsertExecutiveSectionMapping(ExecutiveSectionMapping executiveSectionMapping);
        void UpdateExecutiveSectionMapping(ExecutiveSectionMapping executiveSectionMapping);
        void DeleteExecutiveSectionMapping(int executiveSectionMappingId);
        void DeleteExecutiveSectionMappingRange(ICollection<ExecutiveSectionMapping> executiveSectionMappings);
        void Save();
    }
}