﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.Data.Models
{
    public class Content
    {
        [Key]
        public Guid ContentGuid { get; set; }
        public Enums.ContentTypes ContentType { get; set; }         
        public byte[] ContentBytes { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }

        public DateTime AddedDate { get; set; }

        public int AddedByUserId { get; set; }
        [ForeignKey("AddedByUserId")]
        public virtual UserProfile AddedByUser { get; set; }
        
    }
}