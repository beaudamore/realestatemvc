﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Woolbright.Web.Data.Models
{
    public class Market
    {
        [Key]
        public int MarketId { get; set; }

        [Required]
        [DisplayName("Market Name")]
        public string Name { get; set; }

        public int SortOrder { get; set; }
   
        public virtual ICollection<Property> Properties { get; set; }
     
    }
}