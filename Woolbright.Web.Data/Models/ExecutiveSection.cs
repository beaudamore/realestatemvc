﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Woolbright.Web.Data.Models
{
    public class ExecutiveSection
    {
        [Key]
        public int ExecutiveSectionId { get; set; }
        public string SectionName { get; set; }
        public string SectionBlurb { get; set; }
        public int SortOrder { get; set; }

        public virtual ICollection<ExecutiveSectionMapping> ExecutiveSectionMappings { get; set; }
        

    }
}