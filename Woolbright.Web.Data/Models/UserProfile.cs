﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Woolbright.Web.Data.Models
{
    public class UserProfile
    {
        [Key]
        public int UserProfileId { get; set; }

        public Guid UserProfileGuid { get; set; }

        public int? ExecutiveId { get; set; }
        [ForeignKey("ExecutiveId")]
        public virtual Executive Executive { get; set; }

        [DisplayName("First Name")] 
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Full Name")]
        public string FullName
        {
            get
            {
                return (FirstName + " " + LastName);
            } 
        }

        public string Company { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Cell Phone number")]
        public string CellPhoneNumber { get; set; }

        [DisplayName("Work Phone number")]
        public string WorkPhoneNumber { get; set; }

        [DisplayName("Fax Number")]
        public string FaxNumber { get; set; }

        public string Message { get; set; }

        [DisplayName("Subscribe to Newsletter")]
        public bool SubscribeToNewsletter { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public bool IsActive { get; set; }
    

        // move these into new Prospect class
        //public bool InterestedInQuarterlyNewsletter { get; set; }
        //public bool InterestedInLeasingSpace { get; set; }
        //public string[] CentersInterestedIn { get; set; }
        //public bool InterestedInInvestmentInformation { get; set; }
        //public bool InterestedAcquisitionsInformation { get; set; }
        //public bool InterestedCompanyInformation { get; set; }
        // ==================================

        //public bool IsEmployee { get; set; }
    }
}