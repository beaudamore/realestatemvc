﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Woolbright.Web.Data.Models
{
    public class FreeFormGroup
    {
        [Key]
        public int FreeFormGroupId { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }
        
        public int SortOrder { get; set; }

        public List<FreeFormField> FreeFormFields { get; set; }

    }
}