﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.Data.Models
{
    public class Property
    {
        [Key]
        public int PropertyId { get; set; }

        public Guid ProjectGuid { get; set; }

        [Required]
        [Index("IX_NameAndCity", 1, IsUnique = false)]
        [MaxLength(900)]
        public string Name { get; set; }

        [UIHint("YesNo")]
        public bool NameOverride { get; set; }
        public string NameOverrideValue { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        [Index("IX_NameAndCity", 2, IsUnique = false)]
        [MaxLength(900)]
        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        //[Required]
        public DbGeography Location { get; set; }
        [UIHint("YesNo")]
        public bool LocationOverride { get; set; }
        public DbGeography LocationOverrideValue { get; set; }

        public string LocationDescription { get; set; }
        [UIHint("YesNo")]
        public bool LocationDescriptionOverride { get; set; }
        public string LocationDescriptionOverrideValue { get; set; }
              
        //[Required]
        public string Description { get; set; }
        [UIHint("YesNo")]
        public bool DescriptionOverride { get; set; }
        public string DescriptionOverrideValue { get; set; }

        //[Required]
        public int SquareFootage { get; set; }
        public bool SquareFootageOverride { get; set; }
        public int SquareFootageOverrideValue { get; set; }

        //[Required]
        public int GrossLeasableArea { get; set; }
        [UIHint("YesNo")]
        public bool GrossLeasableAreaOverride { get; set; }
        public int GrossLeasableAreaOverrideValue { get; set; }

        //[Required]
        public int MinDivisible { get; set; }
        [UIHint("YesNo")]
        public bool MinDivisibleOverride { get; set; }
        public int MinDivisibleOverrideValue { get; set; }

        //[Required]
        public int MaxContiguous { get; set; }
        [UIHint("YesNo")]
        public bool MaxContiguousOverride { get; set; }
        public int MaxContiguousOverrideValue { get; set; }

        public string WebsiteUrl { get; set; }

        public int TotalGla { get; set; }
        
        [DisplayName("Fields")]
        public virtual ICollection<FreeFormField> FreeFormFields { get; set; }

        //[Required]
        public int? CorporateContactId { get; set; }
        [ForeignKey("CorporateContactId")]
        [DisplayName("Contact")]
        public virtual UserProfile CorporateContact { get; set; }

        [UIHint("YesNo")]
        public bool CorporateContactOverride { get; set; }
        public virtual UserProfile CorporateContactOverrideValue { get; set; }

        public int? LeasingAgentId { get; set; }
        [ForeignKey("LeasingAgentId")]
        [DisplayName("Leasing Agent")]
        public virtual UserProfile LeasingAgent { get; set; }

        [UIHint("YesNo")]
        public bool LeasingAgentOverride { get; set; }
        public virtual UserProfile LeasingAgentOverrideValue { get; set; }

        [Required]
        public int MarketId { get; set; }
        [ForeignKey("MarketId")]
        [DisplayName("Market")]
        public virtual Market Market { get; set; }

        public Guid? PropertyContentId { get; set; }
        [ForeignKey("PropertyContentId")]
        [DisplayName("Image")]
        public virtual Content PropertyContent { get; set; }

        [UIHint("YesNo")]
        public bool PropertyContentOverride { get; set; }
        public virtual Content PropertyContentOverrideValue { get; set; }

        [InverseProperty("Property")]
        public virtual ICollection<BayOption> BayOptions { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        [UIHint("YesNo")]
        public bool TagsOverride { get; set; }
        public virtual ICollection<Tag> TagsOverrideValue { get; set; }

        public Enums.SiteVisibilityFlags SiteVisibilityFlags { get; set; }
        [UIHint("YesNo")]
        [DisplayName("Override Flags")]
        public bool SiteVisibilityFlagsOverride { get; set; }
        public Enums.SiteVisibilityFlags SiteVisibilityFlagsOverrideValue { get; set; }

        public bool UpdatedSinceImport { get; set; }
         
    }
}