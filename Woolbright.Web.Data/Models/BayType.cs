﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Woolbright.Web.Data.Models
{
    public class BayType
    {
        [Key]
        public Guid BayTypeGuid { get; set; }

        public string Name { get; set; }

        public string HiddenValue { get; set; }

        public bool Active { get; set; }

    }
}