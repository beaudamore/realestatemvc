﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Woolbright.Web.Data.Models
{
    public class Tag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid TagId { get; set; }

        [DisplayName("Tag Name")]
        public string TagName { get; set; }

        public DateTime CreatedDate { get; set; }

        //public Guid ParentTagId { get; set; }
        //[ForeignKey("ParentTagId")]
        //public Tag ParentTag { get; set; }

        public int? PropertyId { get; set; }
        [ForeignKey("PropertyId")]
        public virtual Property Property { get; set; }

        public int? ResearchArticleId { get; set; }
        [ForeignKey("ResearchArticleId")]
        public virtual ResearchArticle ResearchArticle { get; set; }
   
    }
}