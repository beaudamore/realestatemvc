﻿using System;
using System.Collections.Generic;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.Data.Models
{
    public class FreeFormTemplate
    {
        public Guid FreeFormTemplateId { get; set; }

        public string Label { get; set; }

        public Enums.FreeFormValueType ValueType { get; set; }

        public string Suffix { get; set; }
        
        internal ICollection<FreeFormField> FreeFormFields { get; set; }
    }
}