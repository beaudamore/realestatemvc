﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.Data.Models
{
    public class BayOption
    {
        [Key]
        //[Column(Order = 0)]
        public Guid BayGUID { get; set; }
        // from peoms
        public Guid ProjectGUID { get; set; }

        public string Address { get; set; }
        
        // --- SITE VISIBILITY ---
        public Enums.SiteVisibilityFlags SiteVisibilityFlags { get; set; }
        [UIHint("YesNo")]
        [DisplayName("Override")]
        public bool SiteVisibilityFlagsOverride { get; set; } // comes from poems and can be overridden
        public Enums.SiteVisibilityFlags SiteVisibilityFlagsOverrideValue { get; set; }
      
        public string Excerpt { get; set; }
        [UIHint("YesNo")]
        [DisplayName("Override")]
        public bool ExcerptOverride { get; set; } // comes from poems and can be overridden
        [DataType(DataType.MultilineText)]
        public string ExcerptOverrideValue { get; set; } // comes from poems and can be overridden
       

        public string Description { get; set; }
        [UIHint("YesNo")]
        [DisplayName("Override")]
        public bool DescriptionOverride { get; set; } // comes from poems and can be overridden
        [DataType(DataType.MultilineText)]
        public string DescriptionOverrideValue { get; set; } // comes from poems and can be overridden
       
        [UIHint("YesNo")]
        public bool IsFeatured { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool IsFeaturedOverride { get; set; } // comes from poems and can be overridden
        [DisplayName("IsFeatured")]
        public bool IsFeaturedOverrideValue { get; set; } // comes from poems and can be overridden

        [UIHint("YesNo")]
        public bool IsActive { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool IsActiveOverride { get; set; } // comes from poems and can be overridden
        [DisplayName("Active")]
        public bool IsActiveOverrideValue { get; set; } // comes from poems and can be overridden

        [DisplayName("Bay Number")]
        public string BayNumber { get; set; }
        [UIHint("YesNo")]
        [DisplayName("Override")]
        public bool BayNumberOverride { get; set; } // comes from poems and can be overridden
        public string BayNumberOverrideValue { get; set; } // comes from poems and can be overridden
       
        [DisplayName("Bay Size")]
        public int BaySize { get; set; } // comes from poems and can be overridden
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool BaySizeOverride { get; set; } // comes from poems and can be overridden
        public int BaySizeOverrideValue { get; set; } // comes from poems and can be overridden
       
        [DisplayName("Bay Type")]
        public virtual BayType BayType { get; set; } // comes from poems and can be overridden
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool BayTypeOverride { get; set; } // comes from poems and can be overridden
        public virtual BayType BayTypeOverrideValue { get; set; } // comes from poems and can be overridden
       
        public Enums.OccupancyState OccupancyState { get; set; } // comes from poems and can be overridden
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool OccupancyStateOverride { get; set; } // comes from poems and can be overridden
        public Enums.OccupancyState OccupancyStateOverrideValue { get; set; } // comes from poems and can be overridden
        
        public int PropertyId { get; set; }
        [ForeignKey("PropertyId")]
        public virtual Property Property { get; set; }

        [DisplayName("Fields")]
        public virtual ICollection<FreeFormField> FreeFormFields { get; set; }

        public bool UpdatedSinceImport { get; set; }
      

    }
}