﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Woolbright.Web.Data.Models
{
    public class ResearchArticle
    {
        [Key]
        public int ArticleId { get; set; }
        public string ShortDescription { get; set; }
        public string Title { get; set; }
        public string BodyAzureContainerName { get; set; }
        public string BodyRelativeLocalPath { get; set; }
        public DateTime DatePublished { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public int OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public virtual UserProfile Owner { get; set; }

        public virtual string ThumbnailAzureReference { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }


    }
}