﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Woolbright.Web.Data.Models
{
    public class Executive
    {
        [Key]
        public int ExecutiveId { get; set; }

        // every executive is a user
        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile UserProfile { get; set; }

        // exec specific
        public string ExecutiveTitle { get; set; }
        public string ExecutiveBlurb { get; set; }
        
        // main picture
        public Guid ProfilePictureContentId { get; set; }
        [ForeignKey("ProfilePictureContentId")]
        public virtual Content ProfilePictureContent { get; set; }

        // black and white / alternate pic
        public Guid ProfileSidePictureContentId { get; set; }
        [ForeignKey("ProfileSidePictureContentId")]
        public virtual Content ProfileSidePictureContent { get; set; }

        //[InverseProperty("Executives")]
        public virtual ICollection<ExecutiveSectionMapping> ExecutiveSectionMappings { get; set; }  
            
    }
}