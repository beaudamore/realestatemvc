﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Woolbright.Web.Data.Models
{
    public class ExecutiveSectionMapping
    {
        [Key]
        public int Id { get; set; }

        public int SortOrder { get; set; }

        public int ExecutiveSectionId { get; set; }
        [ForeignKey("ExecutiveSectionId")]
        public virtual ExecutiveSection ExecutiveSection { get; set; }

        public int ExecutiveId { get; set; }
        [ForeignKey("ExecutiveId")]
        public virtual Executive Executive { get; set; }

    }
}