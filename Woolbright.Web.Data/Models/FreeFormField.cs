﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.Data.Models
{
    public class FreeFormField
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid FreeFormFieldId { get; set; }

        [Required]
        public Guid FreeFormTemplateId { get; set; }
        [ForeignKey("FreeFormTemplateId ")]
        public virtual FreeFormTemplate FreeFormTemplate { get; set; }

        public int FreeFormGroupId { get; set; }
        [ForeignKey("FreeFormGroupId")]
        public virtual FreeFormGroup FreeFormGroup { get; set; } 

        public string LabelOverride { get; set;  }

        public string StringValue { get; set; }
        public int NumberValue { get; set; }
        public DbGeography Location { get; set; }

        public virtual Content Content { get; set; }        
    
        public int SortOrder { get; set; }

        public Enums.SiteVisibilityFlags SiteVisibilityFlags { get; set; }
        
        
    }
}