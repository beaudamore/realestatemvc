﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WoolbrightCorporateCMS.DAL;

namespace WoolbrightCorporateCMS.Classes
{
    public class AutoCompleteController : Controller
    {
        private CMSContext db = new CMSContext();

        public JsonResult TagSuggestions(string term)
        {
            string[] tags = db.Tags
                .Select(y => y.TagName)
                .Distinct()
                .ToArray();
            return this.Json(tags.Where(t => t.StartsWith(term)), JsonRequestBehavior.AllowGet);
        }        
    }
}
