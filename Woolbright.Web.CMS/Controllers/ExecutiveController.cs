﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ImageResizer;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class ExecutiveController : Controller
    {
        private readonly IExecutiveRepository _executiveRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly IExecutiveSectionRepository _executiveSectionRepository;
        private readonly IExecutiveSectionMappingRepository _executiveSectionMappingRepository;
        private readonly IContentRepository _contentRepository;
        public ExecutiveController(
            IExecutiveRepository executiveRepository,
            IUserProfileRepository userRepository,
            IExecutiveSectionRepository executiveSectionRepository,
            IExecutiveSectionMappingRepository executiveSectionMappingRepository,
            IContentRepository contentRepository)
        {
            _executiveRepository = executiveRepository;
            _userRepository = userRepository;
            _executiveSectionRepository = executiveSectionRepository;
            _executiveSectionMappingRepository = executiveSectionMappingRepository;
            _contentRepository = contentRepository;
        }
     
        // GET: Executive
        public ActionResult Index()
        {
            var executives = _executiveRepository
                .GetExecutives()
                //.OrderBy(x=>x.User.FullName)
                .ToList();
            return View(executives);
        }

        // GET: Executive/Create
        public ActionResult Create()
        {
            var cevm = new CreateExecutiveViewModel
            {
                ExecutiveSections = _executiveSectionRepository.GetExecutiveSections()
                    .OrderBy(x=>x.SortOrder)
                    .ToList(),
                Executives = _executiveRepository.GetExecutives()
                    .ToList()
            };
            FillViewBagForReloadAfterError();
            return View(cevm);
        }

        // POST: Executive/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ExecutiveId,UserProfileId,ExecutiveBlurb,ProfilePictureContentId,ProfileSidePictureContentId")] CreateExecutiveViewModel executive)
        public ActionResult Create(CreateExecutiveViewModel executive)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                var executiveDomainModel = new Executive();

                // User => Executive relationship
                var u = _userRepository.GetUserProfileById(executive.UserProfileId);
                executiveDomainModel.UserProfileId = executive.UserProfileId;
                executiveDomainModel.UserProfile = u;

                // mappings form view model to domain model
                executiveDomainModel.ExecutiveBlurb = executive.ExecutiveBlurb;
                executiveDomainModel.ExecutiveTitle = executive.ExecutiveTitle;

                // executive sections
                foreach (var esc in executive.ExecutiveSectionChoices)
                {
                    if (esc.IsSelected)
                    {
                        // get the section
                        var es = _executiveSectionRepository.GetExecutiveSectionById(esc.ExecutiveSectionId);
                        // get the mappings for this section and increment sortOrders >= this sortOrder#
                        //List<ExecutiveSectionMapping> lesm = db.ExecutiveSectionMappings.Where(x => x.ExecutiveSectionId == es.ExecutiveSectionId).ToList();
                        foreach (var esm in es.ExecutiveSectionMappings)
                        {
                            if (esm.SortOrder >= esc.SortOrder)
                            {
                                // increment sortOrder
                                esm.SortOrder++;
                            }
                        }
                        // add this ESC
                        var esmToAdd = new ExecutiveSectionMapping
                        {
                            SortOrder = esc.SortOrder,
                            ExecutiveId = u.UserProfileId,
                            ExecutiveSection = es,
                            ExecutiveSectionId = es.ExecutiveSectionId
                        };
                        _executiveSectionMappingRepository.InsertExecutiveSectionMapping(esmToAdd);
                    }
                }

                // === Handle uploaded pics
                // ----------- validate image -----------
                var imageMain = Request.Files["ImageMain"];
                var imageMainProcessed = new MemoryStream();
                var imageSecondary = Request.Files["ImageSecondary"];
                var imageSecondaryProcessed = new MemoryStream();
                // check if blank file
                var imageErrorMessage = string.Empty;
                if (imageMain == null || imageMain.ContentLength <= 0 || imageSecondary == null || imageSecondary.ContentLength <= 0)
                {
                    // BOOM !!!
                    ViewBag.Message = "No Image selected for input.";
                    FillViewBagForReloadAfterError();
                    return View(executive);
                }

                // resize images
                ImageBuilder.Current.Build(imageMain.InputStream, imageMainProcessed,
                    new ResizeSettings(System.Configuration.ConfigurationManager.AppSettings["ImageResizeStringForExecutives"]));
                ImageBuilder.Current.Build(imageSecondary.InputStream, imageSecondaryProcessed,
                    new ResizeSettings(System.Configuration.ConfigurationManager.AppSettings["ImageResizeStringForExecutives"]));

                // --- put both images into domain model ---
                // first pic
                var c1 = new Content()
                {
                    ContentBytes = imageMainProcessed.ToArray(),
                    ContentType = Enums.ContentTypes.Image,
                    AddedByUserId = 1,
                    AddedDate = DateTime.Now
                };
                executiveDomainModel.ProfilePictureContent = c1;
                //executiveDomainModel.ProfilePictureContent = ImageSpecific.ValidateImageContent(imageMainProcessed, out imageErrorMessage);
                executiveDomainModel.ProfilePictureContent.FileName = imageMain.FileName;
                    
                // second pic
                var c2 = new Content()
                {
                    ContentBytes = imageSecondaryProcessed.ToArray(),
                    ContentType = Enums.ContentTypes.Image,
                    AddedByUserId = 1,
                    AddedDate = DateTime.Now
                };
                executiveDomainModel.ProfileSidePictureContent = c2;
                //executiveDomainModel.ProfileSidePictureContent = ImageSpecific.ValidateImageContent(imageSecondary, out imageErrorMessage);
                executiveDomainModel.ProfileSidePictureContent.FileName = imageSecondary.FileName;
                
                // handle if error
                //if (imageErrorMessage.Length > 0) // process failed for some reason
                //{
                //    ViewBag.Message = imageErrorMessage;
                //    FillViewBagForReloadAfterError();
                //    /* Let it save the rest without the image override 
                //         * so we don't need special mappings just to show the incomplete form/values */
                //    return View(executive);// TODO: this will likely fail as mappings are needed to begin edit
                //}
                // === end pics

                _executiveRepository.InsertExecutive(executiveDomainModel);
                u.ExecutiveId = executiveDomainModel.ExecutiveId;

                // save everything
                _executiveRepository.Save();
                _executiveSectionRepository.Save();
                _executiveSectionMappingRepository.Save();
                _userRepository.Save();

                return RedirectToAction("Index");
            }
            
            FillViewBagForReloadAfterError();
            return View(executive); // TODO: this likely needs to change
        }

        // GET: Executive/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var executive = _executiveRepository.GetExecutiveById(idInt);
            if (executive == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProfilePictureContentId = new SelectList(_contentRepository.GetContent(), "ContentId", "ContentId", executive.ProfilePictureContentId);
            ViewBag.ProfileSidePictureContentId = new SelectList(_contentRepository.GetContent(), "ContentId", "ContentId", executive.ProfileSidePictureContentId);
            ViewBag.UserId = new SelectList(_userRepository.GetUserProfiles(), "UserProfileId", "FirstName", executive.UserProfileId);

            // Map to ViewModel
            var viewModel = new EditExecutiveViewModel
            {
                UserFullName = executive.UserProfile.FullName,
                ExecutiveBlurb = executive.ExecutiveBlurb,
                ExecutiveId = executive.ExecutiveId,
                ExecutiveSections = _executiveSectionRepository.GetExecutiveSections().ToList(),
                ExecutiveSectionMappings = executive.ExecutiveSectionMappings,
                ExecutiveTitle = executive.ExecutiveTitle,
                ProfilePictureContent = executive.ProfilePictureContent,
                ProfileSidePictureContent = executive.ProfileSidePictureContent
            };

            return View(viewModel);
        }

        // POST: Executive/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ExecutiveId,UserProfileId,ExecutiveBlurb,ProfilePictureContentId,ProfileSidePictureContentId")] EditExecutiveViewModel executive)
        public ActionResult Edit(EditExecutiveViewModel executive)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                // map to domainModel
                var exec = _executiveRepository.GetExecutiveById(executive.ExecutiveId);
                // for removing all mappings
                var isRemovedFromExecutives = true;
                // section mappings
                foreach (var escvm in executive.ExecutiveSectionChoices)
                {
                    // get all other Executive mappings for this section and shuffle (note '!=' in this Linq)
                    var esmExisting = _executiveSectionMappingRepository
                        .GetExecutiveSectionMappings()
                        .Where(x => x.ExecutiveSectionId == escvm.ExecutiveSectionId 
                            && x.ExecutiveId != executive.ExecutiveId)
                        .ToList();
                    // get existing mapping for this executive
                    var esm = _executiveSectionMappingRepository
                        .GetExecutiveSectionMappings()
                        .Where(x => x.ExecutiveSectionId == escvm.ExecutiveSectionId
                            && x.ExecutiveId == executive.ExecutiveId)
                        .ToList();
                    if (esm.Any())
                    {
                        // Keep track of OLD sortOrder
                        var oldSortOrder = esm.First().SortOrder;
                        // set this one's SortOrder
                        esm.First().SortOrder = escvm.SortOrder;
                        // reorder all others

                        // if IsSelected, add/adjust
                        if (escvm.IsSelected)
                        {
                            // we know this user is still an exec
                            isRemovedFromExecutives = false;

                            for (var index = 0; index < esmExisting.Count; index++)
                            {
                                var esmToModify = esmExisting[index];
                                // SHUFFLE: if the new sortorder is greater then the old sortorder
                                if (escvm.SortOrder > oldSortOrder)
                                {
                                    if (esmToModify.SortOrder > oldSortOrder && esmToModify.SortOrder <= escvm.SortOrder)
                                        esmToModify.SortOrder--;
                                    _executiveSectionMappingRepository.UpdateExecutiveSectionMapping(esmToModify);
                                }
                                else if (escvm.SortOrder < oldSortOrder)
                                {
                                    if (esmToModify.SortOrder < oldSortOrder && esmToModify.SortOrder >= escvm.SortOrder)
                                        esmToModify.SortOrder++;
                                    _executiveSectionMappingRepository.UpdateExecutiveSectionMapping(esmToModify);
                                }
                            }
                        }
                        else // IsSelected isn't checked AND the mapping DOES exist, remove the reference
                        {
                            // remove it
                            _executiveSectionMappingRepository.DeleteExecutiveSectionMapping(esm.First().Id);
                            // shuffle existing
                            foreach (var esmToModify in esmExisting)
                            {
                                if (esmToModify.SortOrder >= escvm.SortOrder)
                                {
                                    esmToModify.SortOrder--;
                                }
                            }
                        }
                    }
                    else // mapping doesn't already exist
                    {
                        if (escvm.IsSelected)
                        {
                            // we know this user is now an exec
                            isRemovedFromExecutives = false;
                            // create mapping
                            var esmNew = new ExecutiveSectionMapping
                            {
                                ExecutiveId = executive.ExecutiveId,
                                ExecutiveSectionId = escvm.ExecutiveSectionId,
                                SortOrder = escvm.SortOrder
                            };
                            _executiveSectionMappingRepository.InsertExecutiveSectionMapping(esmNew);
                            // shuffle
                            foreach (var esmToModify in esmExisting)
                            {
                                if (esmToModify.SortOrder >= escvm.SortOrder)
                                {
                                    esmToModify.SortOrder++;
                                }
                            }
                        }
                    }
                }
                // === Handle uploaded pics
                // ----------- validate images -----------
                var imageMain = Request.Files["ImageMain"];
                var imageMainProcessed = new MemoryStream();
                var imageSecondary = Request.Files["ImageSecondary"];
                var imageSecondaryProcessed = new MemoryStream();
                // check if blank file
                var imageErrorMessage = string.Empty;
                // main image
                if (imageMain != null && imageMain.ContentLength > 0)
                {
                    // resize images
                    ImageBuilder.Current.Build(imageMain.InputStream, imageMainProcessed,
                        new ResizeSettings(System.Configuration.ConfigurationManager.AppSettings["ImageResizeStringForExecutives"]));
                    
                    var c1 = new Content()
                    {
                        ContentBytes = imageMainProcessed.ToArray(),
                        ContentType = Enums.ContentTypes.Image,
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now
                    };
                    exec.ProfilePictureContent = c1;
                
                    //exec.ProfilePictureContent = ImageSpecific.ValidateImageContent(imageMain, out imageErrorMessage);
                    exec.ProfilePictureContent.MimeType = imageMain.ContentType;
                    exec.ProfilePictureContent.FileName = imageMain.FileName;
                    // handle if error
                    //if (imageErrorMessage.Length > 0) // process failed for some reason
                    //{
                    //    ViewBag.Message = imageErrorMessage;
                    //    // TODO: handle for error
                    //    //return View(property);// TODO: this will likely fail as mappings are needed to begin edit
                    //}
                }
                // secondary image
                if (imageSecondary != null && imageSecondary.ContentLength > 0)
                {
                    ImageBuilder.Current.Build(imageSecondary.InputStream, imageSecondaryProcessed,
                       new ResizeSettings(System.Configuration.ConfigurationManager.AppSettings["ImageResizeStringForExecutives"]));

                    var c2 = new Content()
                    {
                        ContentBytes = imageSecondaryProcessed.ToArray(),
                        ContentType = Enums.ContentTypes.Image,
                        AddedByUserId = 1,
                        AddedDate = DateTime.Now
                    };
                    exec.ProfileSidePictureContent = c2;
                    //exec.ProfileSidePictureContent = ImageSpecific.ValidateImageContent(imageSecondary, out imageErrorMessage);
                    exec.ProfileSidePictureContent.MimeType = imageSecondary.ContentType;
                    exec.ProfileSidePictureContent.FileName = imageSecondary.FileName;
                    // handle if error
                    //if (imageErrorMessage.Length > 0) // process failed for some reason
                    //{
                    //    ViewBag.Message = imageErrorMessage;
                    //    // TODO : handle for error
                    //    //return View(property);// TODO: this will likely fail as mappings are needed to begin edit
                    //}
                }
                // === end pics

                // remove from execs and reorder
                if (isRemovedFromExecutives)
                {
                    _executiveRepository.DeleteExecutive(exec.ExecutiveId);
                    //User u = db.Users.Where(x => x.ExecutiveId == exec.ExecutiveId).First();
                    var u = _userRepository
                        .GetUserProfiles()
                        .FirstOrDefault(x => x.ExecutiveId == exec.ExecutiveId);
                    if (u != null) u.ExecutiveId = null;
                }

                exec.ExecutiveTitle = executive.ExecutiveTitle;
                exec.ExecutiveBlurb = executive.ExecutiveBlurb;

                //db.Entry(executive).State = EntityState.Modified;
                _executiveSectionMappingRepository.Save();
                _executiveSectionRepository.Save();
                _executiveRepository.Save();
                _userRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(_userRepository.GetUserProfiles(), "UserProfileId", "FirstName", executive.UserId);
            return View(executive);
        }

        // GET: Executive/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            Executive executive = _executiveRepository.GetExecutiveById(idInt);
            if (executive == null)
            {
                return HttpNotFound();
            }
            return View(executive);
        }

        // POST: Executive/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var executive = _executiveRepository.GetExecutiveById(id);
            // remove reference from User also as EF isn't doing this the way it's setup now.
            var u = _userRepository
                .GetUserProfiles()
                .FirstOrDefault(x => x.ExecutiveId == executive.UserProfileId);
            if (u != null) u.ExecutiveId = null;
            // remove mappings
            _executiveSectionMappingRepository.DeleteExecutiveSectionMappingRange(executive.ExecutiveSectionMappings);
            // remove executive
            _executiveRepository.DeleteExecutive(id);
            // save
            _executiveSectionMappingRepository.Save();
            _executiveRepository.Save();
            return RedirectToAction("Index");
        }

        // method to handle sections
        [HttpGet]
        public ActionResult Section(string id)
        {
            // show section in id
            var execs = _executiveRepository.GetExecutives()
                                            .ToList()
                                            .Where(x => x.ExecutiveSectionMappings.Select(c => string.Equals(c.ExecutiveSection.SectionName, id.Replace("-", " "), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault())
                                            .OrderBy(x => x.ExecutiveSectionMappings.Select(c => c.SortOrder).FirstOrDefault());
            if (string.IsNullOrEmpty(id)) return View(execs);
            
            ViewBag.Title = id.Replace("-", " ").ToUpper();
            var firstOrDefault = _executiveSectionRepository
                .GetExecutiveSections()
                .FirstOrDefault(x => x.SectionName == id.Replace("-", " "));
            if (firstOrDefault != null)
                ViewBag.Blurb = firstOrDefault
                    .SectionBlurb;
            return View(execs);
        }

        // single method for all executive pages
        //[HttpGet]
        public ActionResult ExecutiveSubMenu()
        {
            // get sections
            var sections = _executiveSectionRepository.GetExecutiveSections()
                .OrderBy(x => x.SortOrder)
                .ToList();
            return PartialView("_ExecutiveSubMenuView", sections);
        }

        private void FillViewBagForReloadAfterError()
        {
            // fill ddl with available users 
            var usersWithoutExecutiveMapping = _userRepository.GetUserProfiles()
                .Where(x => x.ExecutiveId == null && x.IsActive)
                .OrderBy(x => x.FullName);
             
            ViewBag.UserProfileId = new SelectList(usersWithoutExecutiveMapping, "UserProfileId", "FullName");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _executiveRepository.Dispose();
                _userRepository.Dispose();
                _executiveSectionRepository.Dispose();
                _executiveSectionMappingRepository.Dispose();
                _contentRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
