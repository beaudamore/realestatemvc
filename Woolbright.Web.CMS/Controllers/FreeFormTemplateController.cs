﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class FreeFormTemplateController : Controller
    {
        private readonly IFreeFormTemplateRepository _freeFormTemplateRepository;
        public FreeFormTemplateController(
            IFreeFormTemplateRepository freeFormTemplateRepository)
        {
            _freeFormTemplateRepository = freeFormTemplateRepository;
        }  
        // GET: FreeFormTemplate
        public ActionResult Index()
        {
            return View(_freeFormTemplateRepository.GetFreeFormTemplates().ToList());
        }

        // GET: FreeFormTemplate/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            FreeFormTemplate freeFormTemplate = _freeFormTemplateRepository.GetFreeFormTemplateById(idGuid);
            if (freeFormTemplate == null)
            {
                return HttpNotFound();
            }
            return View(freeFormTemplate);
        }

        // GET: FreeFormTemplate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FreeFormTemplate/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        // This is coming from a bootstrap Modal, FYI
        [HttpPost]
        [ValidateAntiForgeryToken] // This works, see JS for more info...
        public ActionResult Create([Bind(Include = "Label,ValueType,Suffix")] FreeFormTemplate freeFormTemplate)
        {
            if (ModelState.IsValid)
            {
                FreeFormTemplate fft = new FreeFormTemplate();
                fft.FreeFormTemplateId = Guid.NewGuid();
                fft.Label = freeFormTemplate.Label;

                Enums.FreeFormValueType myEnum = (Enums.FreeFormValueType)freeFormTemplate.ValueType;
                fft.ValueType = myEnum;
                fft.Suffix = freeFormTemplate.Suffix;

                _freeFormTemplateRepository.InsertFreeFormTemplate(fft);
                _freeFormTemplateRepository.Save();

                // return fresh Templates model
                return Json(_freeFormTemplateRepository.GetFreeFormTemplates()
                    .OrderBy(x => x.Label));
            }
            return Content("0");      
        }

        // GET: FreeFormTemplate/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            FreeFormTemplate freeFormTemplate = _freeFormTemplateRepository.GetFreeFormTemplateById(idGuid);
            if (freeFormTemplate == null)
            {
                return HttpNotFound();
            }
            return View(freeFormTemplate);
        }

        // POST: FreeFormTemplate/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FreeFormTemplateId,Label,ValueType,Suffix")] FreeFormTemplate freeFormTemplate)
        {
            if (ModelState.IsValid)
            {
                _freeFormTemplateRepository.UpdateFreeFormTemplate(freeFormTemplate);
                _freeFormTemplateRepository.Save();
                return RedirectToAction("Index");
            }
            return View(freeFormTemplate);
        }

        // GET: FreeFormTemplate/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            FreeFormTemplate freeFormTemplate = _freeFormTemplateRepository.GetFreeFormTemplateById(idGuid);
            if (freeFormTemplate == null)
            {
                return HttpNotFound();
            }
            return View(freeFormTemplate);
        }

        // POST: FreeFormTemplate/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            FreeFormTemplate freeFormTemplate = _freeFormTemplateRepository.GetFreeFormTemplateById(id);
            _freeFormTemplateRepository.DeleteFreeFormTemplate(freeFormTemplate.FreeFormTemplateId);
            _freeFormTemplateRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _freeFormTemplateRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
