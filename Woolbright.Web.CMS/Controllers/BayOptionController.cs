﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class BayOptionController : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IBayOptionRepository _bayOptionRepository;
        public BayOptionController(
            IPropertyRepository propertyRepository, 
            IBayOptionRepository bayOptionRepository
            )
        {
            _propertyRepository = propertyRepository;
            _bayOptionRepository = bayOptionRepository;
        }

        // GET: BayOption
        public ActionResult Index()
        {
            var bayOptions = _bayOptionRepository.GetBayOptions();
                //.Include(b => b.Property);
            return View(bayOptions.ToList());
        }

        // GET: BayOption/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            var bayOption = _bayOptionRepository.GetBayOptionById(idGuid);
            if (bayOption == null)
            {
                return HttpNotFound();
            }
            return View(bayOption);
        }

        // GET: BayOption/Create
        public ActionResult Create(int id)// Only way to create a bay is to know the property it will go too...
        {            
            var boVm = new CreateBayOptionViewModel();
            var propertyToAddBayToo = _propertyRepository
                                                    .GetProperties()
                                                    .SingleOrDefault(x => x.PropertyId == id);
            boVm.PropertyNameAndCityAndState = propertyToAddBayToo.Name + " in " + propertyToAddBayToo.City + ", " + propertyToAddBayToo.State;
            boVm.PropertyId = id;
            return View(boVm);
            //ViewBag.PropertyId = new SelectList(db.Properties, "PropertyID", "Name");
            //return View();
        }

        // POST: BayOption/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "BayGUID,ProjectGUID,SiteVisibilityFlags,Excerpt,Description,IsFeatured,BayNumber,BaySize,BayType,OccupancyState,OccupancyStateIsOverridden,OccupancyStateValue,PropertyId")] CreateBayOptionViewModel bayOption)
        public ActionResult Create(CreateBayOptionViewModel bayOption)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                // Map over to domain object
                var domainBayOption = new BayOption
                {
                    BayGUID = Guid.NewGuid(),
                    PropertyId = bayOption.PropertyId,
                    BayNumber = bayOption.BayNumber,
                    Description = bayOption.Description,
                    Excerpt = bayOption.Excerpt,
                    IsFeatured = bayOption.IsFeatured
                };

                // string to int
                var baySizeInt = 0;
                var baySizeTest = int.TryParse(bayOption.BaySize, out baySizeInt);
                if (baySizeTest)
                    domainBayOption.BaySize = baySizeInt;

                // string to int
                var bayTypeInt = 0;
                int.TryParse(bayOption.BayType, out bayTypeInt);
                //if (bayTypeTest)
                  //  domainBayOption.BayType = bayTypeInt;
                //TODO: bayType?

                // Occupancy TODO: occupancy?
                //domainBayOption.OccupancyState = bayOption.OccupancyState;
                //domainBayOption.OccupancyStateIsOverridden = bayOption.OccupancyStateIsOverridden;
                //domainBayOption.OccupancyStateValue = bayOption.OccupancyStateValue;               

                // Site Visibility Flags        
                var siteVisibilityTotalValue = 0;
                foreach (var svf in bayOption.SiteVisibilityFlags)
                {
                    Enums.SiteVisibilityFlags flagTester;
                    if (Enum.TryParse(svf, out flagTester))
                    {
                        // sum up values to get total to them convert to enum
                        siteVisibilityTotalValue += (int)flagTester;
                    }
                }
                // be sure to account for nothing checked ('1' is 'None', not '0')
                if (siteVisibilityTotalValue == 0)
                    siteVisibilityTotalValue = 1;             
                // convert total to Enum
                domainBayOption.SiteVisibilityFlags = (Enums.SiteVisibilityFlags)siteVisibilityTotalValue;

                // add and save
                _bayOptionRepository.InsertBayOption(domainBayOption);
                _bayOptionRepository.Save();
                // redirect back to ProperyyController/Details/id
                return RedirectToAction("Details", "Property", new { id = bayOption.PropertyId });
                //return RedirectToAction("Index");
            }

            ViewBag.PropertyId = new SelectList(_propertyRepository.GetProperties(), "PropertyID", "Name", bayOption.PropertyId);
            return View(bayOption);
        }

        // GET: BayOption/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            var bayOption = _bayOptionRepository.GetBayOptionById(idGuid);
            if (bayOption == null)
            {
                return HttpNotFound();
            }
            ViewBag.PropertyId = new SelectList(_propertyRepository.GetProperties(), "PropertyID", "Name", bayOption.PropertyId);
            return View(bayOption);
        }

        // POST: BayOption/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BayGUID,ProjectGUID,SiteVisibility,Excerpt,Description,IsFeatured,BayNumber,BaySize,BayType,OccupancyState,OccupancyStateIsOverridden,OccupancyStateValue,PropertyId")] BayOption bayOption)
        {
            if (ModelState.IsValid)
            {
                _bayOptionRepository.UpdateBayOption(bayOption);
                //db.Entry(bayOption).State = EntityState.Modified;
                _bayOptionRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.PropertyId = new SelectList(_propertyRepository.GetProperties(), "PropertyID", "Name", bayOption.PropertyId);
            return View(bayOption);
        }

        // GET: BayOption/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idGuid = Guid.Parse(id.ToString());
            var bayOption = _bayOptionRepository.GetBayOptionById(idGuid);
            if (bayOption == null)
            {
                return HttpNotFound();
            }
            return View(bayOption);
        }

        // POST: BayOption/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            //BayOption bayOption = bayOptionRepository.GetBayOptionById(id);
            _bayOptionRepository.DeleteBayOption(id);
            _bayOptionRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _propertyRepository.Dispose();
                _bayOptionRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
