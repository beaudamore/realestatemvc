﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class MarketController : Controller
    {
        //private CMSContext db = new CMSContext();
        private readonly IMarketRepository _marketRepository;
        public MarketController(
            IMarketRepository marketRepository
            )
        {
            _marketRepository = marketRepository;
        }   

        // GET: Market
        public ActionResult Index()
        {
            return View(_marketRepository.GetMarkets()
                .OrderBy(x=>x.SortOrder)
                .ToList());
        }

        // GET: Market/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var market = _marketRepository.GetMarketById(idInt);
            if (market == null)
            {
                return HttpNotFound();
            }
            return View(market);
        }

        // GET: Market/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Market/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MarketID,Name")] Market market)
        {
            if (!ModelState.IsValid) return View(market);
            _marketRepository.InsertMarket(market);
            _marketRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Market/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var market = _marketRepository.GetMarketById(idInt);
            if (market == null)
            {
                return HttpNotFound();
            }
            return View(market);
        }

        // POST: Market/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MarketID,Name,SortOrder")] Market market)
        {
            if (!ModelState.IsValid) return View(market);
            _marketRepository.UpdateMarket(market);
            _marketRepository.Save();
            return RedirectToAction("Index");
        }

        // GET: Market/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var market = _marketRepository.GetMarketById(idInt);
            if (market == null)
            {
                return HttpNotFound();
            }
            return View(market);
        }

        // POST: Market/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //Market market = marketRepository.GetMarketById(id);
            _marketRepository.DeleteMarket(id);
            _marketRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _marketRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
