﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Hosting;
using System.Web.Mvc;
using HtmlAgilityPack;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class ResearchArticleController : Controller
    {
        //private CMSContext db = new CMSContext();
        private readonly IResearchArticleRepository _researchArticleRepository;
        private readonly IContentRepository _contentRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IUserProfileRepository _userRepository;
        public ResearchArticleController(
            IResearchArticleRepository researchArticleRepository,
            IContentRepository contentRepository, 
            ITagRepository tagRepository, 
            IUserProfileRepository userRepository
            )
        {
            _researchArticleRepository = researchArticleRepository;
            _contentRepository = contentRepository;
            _tagRepository = tagRepository;
            _userRepository = userRepository;
        }

        // GET: ResearchArticle
        public ActionResult Index()
        {
            // viewmodel
            var viewModel = new ArchiveResearchArticleViewModel
            {
                ArchiveList = GetArchiveList(),
                Articles = _researchArticleRepository.GetResearchArticles()
                    .Where(x => x.DatePublished.Year == DateTime.Today.Year
                                && x.DatePublished.Month == DateTime.Today.Month)
                    .ToList()
            };
            // TODO: if none this month, use last month's?...          
            return View(viewModel);
        }

        public ActionResult Archive(string id)// this is in format 'yyyymm'
        {
            // viewmodel to return
            var viewModel = new ArchiveResearchArticleViewModel();
            // parse date sent
            var postedDateInfo = id.Split('-');
            var month = (int.Parse(postedDateInfo[0]));
            var year = (int.Parse(postedDateInfo[1]));
            // get articles
            viewModel.Articles = _researchArticleRepository.GetResearchArticles()
                //.Include(r => r.Owner)
                .Where(x => x.DatePublished.Month == month && x.DatePublished.Year == year)
                .ToList();          
            // get archives
            viewModel.ArchiveList = GetArchiveList();
         
            return View("Index", viewModel);
        }

        private Dictionary<string, int> GetArchiveList()
        {
            var grouped = (from p in _researchArticleRepository.GetResearchArticles()
                           group p by new { month = p.DatePublished.Month, year = p.DatePublished.Year } into d
                           select new { dt = d.Key.month + "-" + d.Key.year, count = d.Count() }).OrderByDescending(g => g.dt);
            return grouped.ToDictionary(item => item.dt, item => item.count);
        }


        // GET: ResearchArticle/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var researchArticle = _researchArticleRepository.GetResearchArticleById(idInt);
            if (researchArticle == null)
            {
                return HttpNotFound();
            }
            return View(researchArticle);
        }

        // GET: ResearchArticle/Create
        public ActionResult Create()
        {
            var cravm = new CreateResearchArticleViewModel();
            // load available tags for autocomplete
            cravm.Tags = _tagRepository.GetTags()
                .OrderBy(x => x.TagName).ToList();

            FillViewBagBodyFolderChoices();

            return View(cravm);
        }

        private void FillViewBagBodyFolderChoices(string chosenDirectory = null)
        {
            // read available content folders from FTP upload root
            var path =
                HttpContext.Server.MapPath(ConfigurationManager.AppSettings["ResearchArticleFTPUploadRoot"]);
            var subfolders = Directory.GetDirectories(path)
                .OrderBy(x => new DirectoryInfo(x).Name)
                .ToDictionary(v => v, x => new DirectoryInfo(x).Name); // This gets both the full path and the plain directory name
            ViewBag.BodyFolderChoices = !string.IsNullOrEmpty(chosenDirectory)
               ? new SelectList(subfolders, "Value", "Value", chosenDirectory)
               : new SelectList(subfolders, "Value", "Value"); // right now, we only need the directory name to mappath locally per server
        }

        // POST: ResearchArticle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ArticleId,Title,Body,DatePublished,OwnerId")] ResearchArticle researchArticle)
        public ActionResult Create(CreateResearchArticleViewModel researchArticle)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            // refill autocomplete regardless if modelstate valid or not
            researchArticle.Tags = _tagRepository.GetTags()
                   .OrderBy(x => x.TagName).ToList();

            // bad model state, return
            if (!ModelState.IsValid) return View(researchArticle);
            
            // make sure that no article exists with this name-date combo
            var dup = _researchArticleRepository.GetResearchArticles()
                .Where(x => x.Title == researchArticle.Title && x.DatePublished == researchArticle.PublishDate);
            if (dup.Any())
            {
                ViewBag.ErrorMessage =
                    "Title/Publish Date combo already in use. Please use a different Title/Publish Date combo.";
                return View(researchArticle);
            }

            var ra = new ResearchArticle
            {
                OwnerId = 1, // TODO: fix this when we go to AD
                Title = researchArticle.Title,
                ShortDescription = researchArticle.ShortDescription,
                DatePublished = researchArticle.PublishDate,
                LastModifiedDate = DateTime.Now
            };
            _researchArticleRepository.InsertResearchArticle(ra);
            _researchArticleRepository.Save(); // to force a new id

            // container name
            ra.BodyAzureContainerName = ConfigurationManager.AppSettings["ResearchArticleBodyAzureContainerNameBase"] +
                                        ra.ArticleId;
            // ---grab the body content files for azure---
            // local path
            ra.BodyRelativeLocalPath = researchArticle.BodyFolderChoices;
            // get filepaths
            var filePaths = GetFilePathsAndUpdateIndexHtml(researchArticle.BodyFolderChoices, ra.BodyAzureContainerName);
            // handle thumbnail - even though 'RequestFilesToContentList' returns a list, we know there's only one image/thumb
            string errorMessage;
            var contentList = ContentSpecific.RequestFilesToContentList(Request.Files, out errorMessage);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                // handle error here.
            }
            // wad up into an Azure obj...
            var lastModifiedDateForIndexHtml = DateTime.Now;
            ra.ThumbnailAzureReference = Azure.PostBlobsToAzure(ra.BodyAzureContainerName, filePaths, contentList[0].ContentBytes, lastModifiedDateForIndexHtml);
           
            // delete everything in the FTP Upload folder after upload. 
            // (since we're modifying the index.html pre-upload it will cause issues later)
            DirectoryInfo downloadedMessageInfo = new DirectoryInfo(HostingEnvironment.MapPath(ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] +
                    researchArticle.BodyFolderChoices));

            foreach (FileInfo file in downloadedMessageInfo.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
            {
                dir.Delete(true);
            }
            // maybe leave the directory in case of edit? or remove because it might be confusing later?
            //downloadedMessageInfo.Delete();

            // now the comma delimited tag list
            var tagList = new List<Tag>();
            if (!string.IsNullOrEmpty(researchArticle.TagsOverrideValue))
            {
                var tagArray = researchArticle.TagsOverrideValue.Trim().Split(',');
                tagList.AddRange(from t1 in tagArray
                    where t1.Trim().Length > 0
                    select new Tag
                    {
                        TagId = Guid.NewGuid(), 
                        TagName = t1.Trim(), 
                        CreatedDate = DateTime.Now, 
                        ResearchArticle = ra
                    });
            }
            // if anything entered besides commas and blanks, add it
            if (tagList.Count > 0)
                ra.Tags = tagList;
            
            _researchArticleRepository.UpdateResearchArticle(ra);
            _researchArticleRepository.Save();
            
            FillViewBagBodyFolderChoices();

            return RedirectToAction("Index");
            //ViewBag.OwnerId = new SelectList(db.Users, "UserProfileId", "FirstName", researchArticle.OwnerId);
        }

        // GET: ResearchArticle/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var researchArticle = _researchArticleRepository.GetResearchArticleById(idInt);
            if (researchArticle == null)
            {
                return HttpNotFound();
            }
            var viewModel = new EditResearchArticleViewModel
            {
                ArticleId = researchArticle.ArticleId,
                BodyUrl = researchArticle.BodyAzureContainerName,// TODO: set this to be the selected folder
                BodyPathLocal = researchArticle.BodyRelativeLocalPath,
                ShortDescription = researchArticle.ShortDescription,
                ThumbnailAzureReference = researchArticle.ThumbnailAzureReference,
                DatePublished = researchArticle.DatePublished,
                Owner = researchArticle.Owner,
                Title = researchArticle.Title,
                Tags = researchArticle.Tags
            };

            // refill bodyFolderChoices DDL
            FillViewBagBodyFolderChoices(researchArticle.BodyRelativeLocalPath);

            //ViewBag.OwnerId = new SelectList(db.Users, "UserProfileId", "FirstName", researchArticle.OwnerId);
            return View(viewModel);
        }

        // POST: ResearchArticle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ArticleId,Title,Body,DatePublished,OwnerId")] ResearchArticle researchArticle)
        public ActionResult Edit(EditResearchArticleViewModel researchArticle)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                // get article to work with
                var ra = _researchArticleRepository
                    .GetResearchArticles()
                    .FirstOrDefault(x => x.ArticleId == researchArticle.ArticleId);
                if (ra != null)
                {
                    ra.Title = researchArticle.Title;
                    ra.ShortDescription = researchArticle.ShortDescription;

                    // handle tags
                    var tagList = new List<Tag>();
                    if (!string.IsNullOrEmpty(researchArticle.TagsOverrideValue)) // catch empty spaces and only insert if checked...
                    {
                        // delete existing tags
                        var tagsToDelete = _tagRepository.GetTags()
                            .Where(x => x.ResearchArticleId == researchArticle.ArticleId).ToList().FirstOrDefault();
                        if (tagsToDelete != null)
                        {
                            _tagRepository.DeleteTag(tagsToDelete.TagId);
                        }
                        // now the comma delimited tag list
                        var tagArray = researchArticle.TagsOverrideValue.Trim().Split(',');
                        for (var i = 0; i < tagArray.Length; i++)
                        {
                            if (tagArray[i].Trim().Length > 0) // catch blanks and just commas
                            {
                                var t = new Tag
                                {
                                    TagId = Guid.NewGuid(),
                                    TagName = tagArray[i].Trim(),
                                    CreatedDate = DateTime.Now,
                                    ResearchArticle = ra
                                };
                                tagList.Add(t);
                            }
                        }
                        // if anything entered besides commas and blanks, add it
                        if (tagList.Count > 0)
                            ra.Tags = tagList;
                    }
                    // ---azure---
                    // get filepaths
                    var filePaths = GetFilePathsAndUpdateIndexHtml(researchArticle.BodyFolderChoices[0], ra.BodyAzureContainerName);
                    // handle thumbnail - even though 'RequestFilesToContentList' returns a list, we know there's only one image/thumb
                    string errorMessage;
                    var contentList = ContentSpecific.RequestFilesToContentList(Request.Files, out errorMessage);
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        // handle error here.
                    }

                    // wad up into an Azure obj...
                    var lastModifiedDateForIndexHtml = DateTime.Now;
                    var thumbAzureRef = Azure.PostBlobsToAzure(ra.BodyAzureContainerName, filePaths, contentList[0].ContentBytes, lastModifiedDateForIndexHtml);
                    if (thumbAzureRef.Length > 0)
                    {
                        ra.ThumbnailAzureReference = thumbAzureRef;
                    }
                }
                // end content
                _contentRepository.Save();
                _researchArticleRepository.Save();
                _tagRepository.Save();

                return RedirectToAction("Index");
            }
            // reset viewBag
            // refill bodyFolderChoices DDL
            FillViewBagBodyFolderChoices();
            ViewBag.OwnerId = new SelectList(_userRepository.GetUserProfiles(), "UserProfileId", "FirstName", researchArticle.OwnerId);
     
            return View(researchArticle);
        }


        private List<string> GetFilePathsAndUpdateIndexHtml(string bodyFolderChoices, string containerName)
        {
            // get path to each item
            var filePaths =
                Directory.GetFiles(
                    Server.MapPath(ConfigurationManager.AppSettings["ResearchArticleFTPUploadRoot"] + "/" +
                                   bodyFolderChoices));
            // get root from web.config
            var azureRootUrl = ConfigurationManager.AppSettings["AzureBlobRootUrl"] + containerName + "/";
            // find index.html and replace relative references
            foreach (var s in filePaths)
            {
                if (s.Contains("index.html"))
                {
                    var doc = new HtmlDocument();
                    doc.LoadHtml(System.IO.File.ReadAllText(s));
                    HtmlNodeCollection links = doc.DocumentNode.SelectNodes("//*[@background or @lowsrc or @src or @href]");
                    if (links == null)
                        continue;
                    foreach (HtmlNode link in links)
                    {
                        // references to outside URLs this will break unless we check for 'http' and leave alone
                        if (link.Attributes["background"] != null && !link.Attributes["background"].Value.Contains("http"))
                            link.Attributes["background"].Value = azureRootUrl + link.Attributes["background"].Value;
                        if (link.Attributes["href"] != null && !link.Attributes["href"].Value.Contains("http"))
                            link.Attributes["href"].Value = azureRootUrl + link.Attributes["href"].Value;
                        if (link.Attributes["lowsrc"] != null && !link.Attributes["lowsrc"].Value.Contains("http"))
                            link.Attributes["lowsrc"].Value = azureRootUrl + link.Attributes["lowsrc"].Value;
                        if (link.Attributes["src"] != null && !link.Attributes["src"].Value.Contains("http"))
                            link.Attributes["src"].Value = azureRootUrl + link.Attributes["src"].Value;
                    }
                    doc.Save(s);
                }
            }
            return filePaths.ToList();
        }

        // GET: ResearchArticle/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var researchArticle = _researchArticleRepository.GetResearchArticleById(idInt);
            if (researchArticle == null)
            {
                return HttpNotFound();
            }
            return View(researchArticle);
        }

        // POST: ResearchArticle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            // delete container from azure
            ResearchArticle researchArticle = _researchArticleRepository.GetResearchArticleById(id);
            Azure.DeleteContainer(researchArticle.BodyAzureContainerName);

            _researchArticleRepository.DeleteResearchArticle(id);
            _researchArticleRepository.Save();
            return RedirectToAction("Index");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult RemoveContentItem(int researchArticleId, int contentId)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var ra = _researchArticleRepository
        //            .GetResearchArticles()
        //            .FirstOrDefault(x => x.ArticleId == researchArticleId);
        //        var c = _contentRepository.GetContentById(contentId);
        //        //if (ra != null) ra.Thumbnail.Remove(c);
        //        _contentRepository.DeleteContent(contentId);
        //        _researchArticleRepository.Save();
        //        _contentRepository.Save();
        //    }
        //    else
        //    {
        //        ViewBag.ErrorMessage = "Something went wrong."; // TODO flesh this out
        //    }
        //    return RedirectToAction("Edit", new { id = researchArticleId });
        //}
                
        [HttpGet]
        public FileResult RetrieveFile(Guid id)
        {
            var content = _contentRepository.GetContentById(id);
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + content.FileName + "");
            return File(content.ContentBytes, content.MimeType);
        }

        //[HttpGet]
        //public ActionResult GetContentByArticleId(int id)
        //{
        //    var article = _researchArticleRepository
        //        .GetResearchArticles()
        //        .FirstOrDefault(x => x.ArticleId == id);
        //    return PartialView("_ResearchArticleContentView", article.Thumbnail);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _researchArticleRepository.Dispose();
                _contentRepository.Dispose();
                _tagRepository.Dispose();
                _userRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
