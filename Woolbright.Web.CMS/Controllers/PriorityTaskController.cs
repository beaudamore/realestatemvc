﻿using System.Linq;
using System.Web.Mvc;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.CMS.Controllers
{
    public class PriorityTaskController : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
      
        public PriorityTaskController(
            IPropertyRepository propertyRepository
            )
        {
            _propertyRepository = propertyRepository;
        }

        // GET: PriorityTask
        public ActionResult Index()
        {
            var viewModel = new PriorityTasksViewModel
            {
                Properties = _propertyRepository.GetProperties()
                    .Where(x => !x.UpdatedSinceImport || x.BayOptions.Any(y => !y.UpdatedSinceImport)).ToList()
                    .OrderBy(x => x.Name)
                    .Select(
                        x => new PriorityTasksPropertiesViewModel() {PropertyId = x.PropertyId, PropertyName = x.Name})
                    .ToList()
            };
            return PartialView("_PriorityTasks", viewModel);
        }

       
    }
}
