﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class PropertyController : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IMarketRepository _marketRepository;
        private readonly IFreeFormTemplateRepository _freeFormTemplateRepository;
        private readonly IFreeFormGroupRepository _freeFormGroupRepository;
        private readonly ITagRepository _tagRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IBayOptionRepository _bayOptionRepository;
        private readonly IBayTypeRepository _bayTypeRepository;
    
        public PropertyController(
            IPropertyRepository propertyRepository, 
            IMarketRepository marketRepository,
            IFreeFormTemplateRepository freeFormTemplateRepository,
            IFreeFormGroupRepository freeFormGroupRepository,
            ITagRepository tagRepository,
            IUserProfileRepository userRepository,
            IContentRepository contentRepository,
            IBayOptionRepository bayOptionRepository,
            IBayTypeRepository bayTypeRepository
            )
        {
            _propertyRepository = propertyRepository;
            _marketRepository = marketRepository;
            _freeFormTemplateRepository = freeFormTemplateRepository;
            _freeFormGroupRepository = freeFormGroupRepository;
            _tagRepository = tagRepository;
            _userRepository = userRepository;
            _contentRepository = contentRepository;
            _bayOptionRepository = bayOptionRepository;
            _bayTypeRepository = bayTypeRepository;
        }

        // GET: Property
        public ActionResult Index()
        {
            // Use view model
            var propList = _propertyRepository.GetProperties()
                .OrderBy(x=>x.Name)
                .ToList();
            var pDvMs = propList.Select(property => new PropertyDetailsViewModel
            {
                PropertyId = property.PropertyId, 
                Address = property.Address, 
                Address2 = property.Address2, 
                City = property.City, 
                State = property.State, 
                Zip = property.Zip, 
                Name = property.Name, 
                Description = property.Description, 
                Market = property.Market, 
                GrossLeasableArea = property.GrossLeasableArea, 
                MaxContiguous = property.MaxContiguous, 
                MinDivisible = property.MinDivisible, 
                SquareFootage = property.SquareFootage, 
                WebsiteUrl = property.WebsiteUrl, 
                FreeFormFields = property.FreeFormFields, 
                CorporateContact = property.CorporateContact, 
                LeasingAgent = property.LeasingAgent,
                PropertyContent = property.PropertyContent, 
                Location = property.Location, 
                Tags = property.Tags.GroupBy(x => x.TagName).Select(y => y.First()).ToList(), 
                SiteVisibilityFlags = property.SiteVisibilityFlags, 
                BayOptions = property.BayOptions
            }).ToList();
            return View(pDvMs);
        }

        // GET: Property/Details/5
        public ActionResult Details(int id)
        {            
            var property = _propertyRepository.GetPropertyById(id);
            if (property == null)
            {
                return HttpNotFound();
            }
            // Use View Model to display Details
            var pDvm = new PropertyDetailsViewModel
            {
                PropertyId = id,
                Address = property.Address,
                Address2 = property.Address2,
                City = property.City,
                State = property.State,
                Zip = property.Zip,
                Name = property.Name,
                Description = property.Description,
                Market = property.Market,
                GrossLeasableArea = property.GrossLeasableArea,
                MaxContiguous = property.MaxContiguous,
                MinDivisible = property.MinDivisible,
                SquareFootage = property.SquareFootage,
                WebsiteUrl = property.WebsiteUrl,
                FreeFormFields = property.FreeFormFields,
                CorporateContact = property.CorporateContact,
                LeasingAgent = property.LeasingAgent,
                PropertyContent = property.PropertyContent,
                Location = property.Location,
                LocationOverride = property.LocationOverride,
                LocationOverrideValue = property.LocationOverrideValue,
                Tags = property.Tags
                    .GroupBy(x => x.TagName)
                    .Select(y => y.First())
                    .ToList(),
                SiteVisibilityFlags = property.SiteVisibilityFlags,
                BayOptions = property.BayOptions
            };
            return View(pDvm);
        }

        // GET: Property/Create
        public ActionResult Create()
        {
            var aView = new EditPropertyViewModel
            {
                Markets = _marketRepository.GetMarkets()
                    .ToList(),
                FreeFormTemplates = _freeFormTemplateRepository.GetFreeFormTemplates()
                    .Where(x =>
                        (x.ValueType == Enums.FreeFormValueType.Number
                         || x.ValueType == Enums.FreeFormValueType.String)
                    )
                    .ToList(),
                FreeFormValueTypes =
                    Enum.GetValues(typeof (Enums.FreeFormValueType)).Cast<Enums.FreeFormValueType>().ToList(),
                Tags = _tagRepository.GetTags()
                    .OrderBy(x => x.TagName)
                    .ToList()
            };
            return View(aView);
        }

        // POST: Property/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "PropertyID,Name,Address,Address2,City,State,Zip,Location,WebsiteURL,Description,SquareFootage,GrossLeasableArea,MinDivisible,MaxContiguous,PropertyTypes,PropertySubTypes,AdditionalSubTypes,PropertyContactId,MarketId,PropertyContentId,FreeFormFieldsToSubmit,Tags")] CreatePropertyViewModel property)
        public ActionResult Create(EditPropertyViewModel property)
        {
            // ReSharper disable once UnusedVariable
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {            
                // ----------- validate image -----------
                var image = Request.Files["Image"];

                // check if blank file
                if (image == null || image.ContentLength <= 0)
                {
                    ViewBag.Message = "No Image selected";
                    return View(property);
                }       
                try
                {
                    // see if it IS an image first...
                    using (var img = Image.FromStream(image.InputStream))
                    {
                        // Use 'AcceptedImageTypes' key in web.config to validate uploaded image type
                        // ==========================================================================
                        var isAcceptedType = false;
                        var imageFormats = new List<ImageFormat>();

                        // get accepted extensions from web/config
                        var acceptedImageTypes = System.Configuration.ConfigurationManager.AppSettings["AcceptedImageTypes"]
                                                                .Split('|')
                                                                .ToList();

                        // convert extension strings to ImageFormats
                        foreach (var s in acceptedImageTypes)
                        {
                            var format = ImageSpecific.GetImageFormat(s);
                            imageFormats.Add(format);
                        }

                        // compare the image to the list of accepted formats
                        foreach (var i in imageFormats)
                        {
                            if (img.RawFormat.Equals(i))
                            {
                                isAcceptedType = true;
                            }
                        }

                        // Let the user know it's not a type in the list of accepted extensions
                        if (!isAcceptedType)
                        {
                            ViewBag.Message = "Wrong Image Type. Please use an image one with the following extenstions:<br /><b>.jpeg, .gif, .png, or .bmp</b>";
                            return View(property);
                        }
                    }
                }
                catch // image from stream failed, not an image file type
                {
                    ViewBag.Message = "Wrong File type. Please use an image one with the following extenstions:<br /><b>.jpeg, .gif, .png, or .bmp</b>";
                    return View(property);
                }

                // check size from app key
                var maxImageUploadSizeInMb = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxImageUploadSizeInMB"]);
                if (image.ContentLength > maxImageUploadSizeInMb * 1024 * 1024) // 5 MB
                {
                    ViewBag.Message = "Image over 5mb. Please upload a smaller image.";
                    return View(property);
                }
             
                /*
                * All good, read image into byte[]
                */
                var target = new MemoryStream();
                image.InputStream.Position = 0; // this line was KEY in solving the 0 bytes problem
                image.InputStream.CopyTo(target);
                var imageBytes = target.ToArray();
                var propertyImage = new Content
                {
                     AddedByUserId = 1
                     ,AddedDate = DateTime.Now
                     ,ContentBytes = imageBytes
                     ,ContentType = (int)Enums.ContentTypes.Image
                };
                // ------------- End Image ----------------
               
                // ------- mappings to Domain Model from View Model ---------
                var domainProperty = new Property
                {
                    PropertyContent = propertyImage,
                    Address = property.Address,
                    Address2 = property.Address2,
                    City = property.City,
                    Description = property.Description,
                    GrossLeasableArea = property.GrossLeasableArea,
                    MarketId = property.MarketId,
                    MaxContiguous = property.MaxContiguous,
                    MinDivisible = property.MinDivisible,
                    Name = property.Name,
                    SquareFootage = property.SquareFootage,
                    State = property.State,
                    WebsiteUrl = property.WebsiteUrl,
                    Zip = property.Zip,
                    SiteVisibilityFlags = DetermineSiteVisibilityFlags(property.SiteVisibilityFlagsToSubmit)
                };
                // FreeFormFields
                var freeFormFieldsToAdd = new List<FreeFormField>();
                // use 'counter' in case they deleted/added fields and SortOrder is non-sequential like 0,1,..3,4..
                var counter = 0;
                if (property.FreeFormFieldsToSubmit != null)
                {
                    foreach (var pfffavm in property.FreeFormFieldsToSubmit.OrderBy(x => x.SortOrder))
                    {
                        // map to FreeFormField objects
                        var fff = new FreeFormField
                        {
                            FreeFormFieldId = Guid.NewGuid(),
                            FreeFormTemplateId = pfffavm.FreeFormTemplateId,
                            FreeFormGroup = _freeFormGroupRepository
                                .GetFreeFormGroups()
                                .First(x => x.Title == "Property"),
                            SortOrder = counter,
                            StringValue = pfffavm.StringValue,
                            NumberValue = pfffavm.NumberValue
                        };
                        freeFormFieldsToAdd.Add(fff);
                        // increment counter
                        counter++;
                    }
                    domainProperty.FreeFormFields = freeFormFieldsToAdd;
                }                
            
                // Location
                var theAddress = property.Address;
                if (!string.IsNullOrEmpty(property.Address2))
                    theAddress += " " + property.Address2;
                theAddress += " " + property.City;
                theAddress += " " + property.State;
                theAddress += " " + property.Zip;
                domainProperty.Location = ContentSpecific.FindCoordinatesFromGoogleMaps(theAddress);  

                // Contact               
                //domainProperty.CorporateContact.UserProfileId = property.CorporateContactOverrideValue; 
               
                // Tags
                var tagList = new List<Tag>();
                if (property.Tags != null && property.TagsOverrideValue.Trim().Length > 0) // catch just spaces...
                {
                    var tagArray = property.TagsOverrideValue.Trim().Split(',');
                    tagList.AddRange(from t1 in tagArray
                        where t1.Trim().Length > 0
                        select new Tag
                        {
                            TagId = Guid.NewGuid(), TagName = t1.Trim(), CreatedDate = DateTime.Now, Property = domainProperty
                        });
                    // OLD PRE-RESHARPER for above ^
                    //for (var i = 0; i < tagArray.Length; i++)
                    //{
                    //    if (tagArray[i].Trim().Length > 0) // catch blanks and just commas
                    //    {
                    //        var t = new Tag
                    //        {
                    //            TagId = Guid.NewGuid(),
                    //            TagName = tagArray[i].Trim(),
                    //            CreatedDate = DateTime.Now,
                    //            Property = domainProperty
                    //        };
                    //        tagList.Add(t);
                    //    }
                    //}
                    if (tagList.Count > 0)
                        domainProperty.Tags = tagList;
                }
                // End Tags
                // -------------- End Mappings ---------------

                // Try adding new Property
                _propertyRepository.InsertProperty(domainProperty);
                try
                {
                    _propertyRepository.Save();
                }
                catch (Exception ex)
                {
                    // log error
                    // LogError(ex);
                    // inform user
                    ViewBag.Message = "Looks like something wasn't filled out correctly.<br />" + ex.Message;
                    return View(property);
                }
                return RedirectToAction("Details", "Property", new { id = domainProperty.PropertyId });
                // If staying on same page to add another use next 2 lines instead of above
                //ViewBag.Message = "Successfully added Property.";
                //return View();
            }

            ViewBag.Message = "Looks like something wasn't filled out correctly.<br />Try again!";
            return View(property);
        }

        // GET: Property/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var domainProperty = _propertyRepository.GetPropertyById(idInt);
            if (domainProperty == null)
            {
                return HttpNotFound();
            }
            // map to view model
            var property = new EditPropertyViewModel();
            property.Name = domainProperty.Name;
            property.NameOverride = domainProperty.NameOverride;
            property.NameOverrideValue = domainProperty.NameOverrideValue;
            property.Address = domainProperty.Address;
            property.Address2 = domainProperty.Address2;
            property.BayOptions = domainProperty.BayOptions.ToList();
            property.City = domainProperty.City;
            property.Description = domainProperty.Description;
            property.DescriptionOverride = domainProperty.DescriptionOverride;
            property.DescriptionOverrideValue = domainProperty.DescriptionOverrideValue;
            property.SiteVisibilityFlagsExisting = domainProperty.SiteVisibilityFlags;
            property.SiteVisibilityFlagsOverride = domainProperty.SiteVisibilityFlagsOverride;
            property.SiteVisibilityFlagsOverrideValue = domainProperty.SiteVisibilityFlagsOverrideValue;
            property.FreeFormFieldsForThisProperty = domainProperty.FreeFormFields;
            property.FreeFormTemplates = _freeFormTemplateRepository.GetFreeFormTemplates()
                .Where(x =>
                    (x.ValueType == Enums.FreeFormValueType.Number
                     || x.ValueType == Enums.FreeFormValueType.String)
                )
                .ToList();
            property.FreeFormValueTypes = Enum.GetValues(typeof (Enums.FreeFormValueType)).Cast<Enums.FreeFormValueType>().ToList();
            property.GrossLeasableArea = domainProperty.GrossLeasableArea;
            property.GrossLeasableAreaOverride = domainProperty.GrossLeasableAreaOverride;
            property.GrossLeasableAreaOverrideValue = domainProperty.GrossLeasableAreaOverrideValue;
            property.Location = domainProperty.Location;
            property.LocationOverride = domainProperty.LocationOverride;
            property.LocationOverrideValue = domainProperty.LocationOverrideValue;
            property.MarketId = domainProperty.Market.MarketId;
            property.MaxContiguous = domainProperty.MaxContiguous;
            property.MaxContiguousOverride = domainProperty.MaxContiguousOverride;
            property.MaxContiguousOverrideValue = domainProperty.MaxContiguousOverrideValue;
            property.MinDivisible = domainProperty.MinDivisible;
            property.MinDivisibleOverride = domainProperty.MinDivisibleOverride;
            property.MinDivisibleOverrideValue = domainProperty.MinDivisibleOverrideValue;
            // corporate contacts
            property.CorporateContacts = _userRepository.GetUserProfiles()
                .Where(x => x.UserProfileId != domainProperty.CorporateContactId)
                .Select(t => new {Value = t.UserProfileId.ToString(), Text = t.LastName + " " + t.FirstName})
                .AsEnumerable()
                .ToDictionary(t => t.Value, t => t.Text);
            // corporate contact
            if (domainProperty.CorporateContact != null)
                property.CorporateContactFullName = domainProperty.CorporateContact.FullName;
            property.CorporateContactOverride = domainProperty.CorporateContactOverride;
            if (domainProperty.CorporateContactOverride && domainProperty.CorporateContactOverrideValue != null)
            {
                property.CorporateContactOverrideValue = domainProperty.CorporateContactOverrideValue.FullName;
            }
            // leasing agent
            if (domainProperty.LeasingAgent != null)
                property.LeasingAgentFullName = domainProperty.LeasingAgent.FullName;
            property.LeasingAgentOverride = domainProperty.LeasingAgentOverride;
            if (domainProperty.LeasingAgentOverride && domainProperty.LeasingAgentOverrideValue != null)
            {
                property.LeasingAgentOverrideValue = domainProperty.LeasingAgentOverrideValue.FullName;
            }
            
            property.UserProfileFullNames = _userRepository.GetUserProfiles()
                .Select(y => y.FullName)
                .ToList();
            property.PropertyContent = domainProperty.PropertyContent;
            property.PropertyContentOverride = domainProperty.PropertyContentOverride;
            property.PropertyContentOverrideValue = domainProperty.PropertyContentOverrideValue;
            property.PropertyId = domainProperty.PropertyId;
            property.SquareFootage = domainProperty.SquareFootage;
            property.SquareFootageOverride = domainProperty.SquareFootageOverride;
            property.SquareFootageOverrideValue = domainProperty.SquareFootageOverrideValue;
            property.State = domainProperty.State;
            property.WebsiteUrl = domainProperty.WebsiteUrl;
            property.Zip = domainProperty.Zip;
            property.Tags = domainProperty.Tags;
            property.TagsExisting = _tagRepository.GetTags()
                .GroupBy(x => x.TagName)
                .Select(y => y.FirstOrDefault())
                .ToList();
            property.TagsOverride = domainProperty.TagsOverride;

            if (domainProperty.TagsOverrideValue != null)
            {
                var tagList = domainProperty.TagsOverrideValue
                    .Select(x => x.TagName).ToArray();
                var sb = new StringBuilder();
                for (var x = 0; x < tagList.Count(); x++)
                {
                    if (x > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(tagList[x]);
                }
                property.TagsOverrideValue = sb.ToString();
            }
            // end Tags

            // fill ddl with available BayTypes for overrides
            property.BayTypes = _bayTypeRepository
                .GetBayTypes()
                .ToDictionary(g => g.Name, g => g.BayTypeGuid.ToString());

            return View(property);
        }

        // POST: Property/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ID,Market,Name,Address,Address2,City,State,Zip,Location,Longitude,WebsiteURL,Description,SquareFootage,GrossLeasableArea,MinDivisible,MaxContiguous,PropertyType,PropertySubType,AdditionalSubTypes,PropertyContactId,PropertyImageContent,PropertyImageName")] EditPropertyViewModel property)
        public ActionResult Edit(EditPropertyViewModel property)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                // mappings
                var domainProperty = _propertyRepository
                    .GetProperties()
                    .First(x => x.PropertyId == property.PropertyId);
                            
                domainProperty.NameOverride = property.NameOverride;
                domainProperty.NameOverrideValue = property.NameOverride ? property.NameOverrideValue : null;
                
                domainProperty.DescriptionOverride = property.DescriptionOverride;
                domainProperty.DescriptionOverrideValue = property.DescriptionOverride ? property.DescriptionOverrideValue : null;
            
                domainProperty.GrossLeasableAreaOverride = property.GrossLeasableAreaOverride;
                domainProperty.GrossLeasableAreaOverrideValue = property.GrossLeasableAreaOverrideValue;
               
                domainProperty.MaxContiguousOverride = property.MaxContiguousOverride;
                domainProperty.MaxContiguousOverrideValue = property.MaxContiguousOverrideValue;
                
                domainProperty.MinDivisibleOverride = property.MinDivisibleOverride;
                domainProperty.MinDivisibleOverrideValue = property.MinDivisibleOverrideValue;
                               
                domainProperty.SiteVisibilityFlagsOverride = property.SiteVisibilityFlagsOverride;
                domainProperty.SiteVisibilityFlagsOverrideValue = property.SiteVisibilityFlagsOverrideValue;
               
                domainProperty.SquareFootageOverride = property.SquareFootageOverride;
                domainProperty.SquareFootageOverrideValue = property.SquareFootageOverrideValue;

                // === image override ===              
                if (property.PropertyContentOverride)
                {
                    // ----------- validate image -----------
                    var image = Request.Files["ImageOverride"];
                    var imageOverrideErrorMessage = string.Empty;
                    // check if blank file
                    if (image == null || image.ContentLength <= 0)
                    {
                        ViewBag.Message = "No Image selected";
                        domainProperty.PropertyContentOverride = false;
                        _contentRepository.DeleteContent(domainProperty.PropertyContentOverrideValue.ContentGuid);
                        domainProperty.PropertyContentOverrideValue = null;
                        //return View(property);
                    }
                    else
                    {
                        domainProperty.PropertyContentOverride = property.PropertyContentOverride;
                        domainProperty.PropertyContentOverrideValue = ImageSpecific.ValidateImageContent(image, out imageOverrideErrorMessage);
                        domainProperty.PropertyContentOverrideValue.FileName = image.FileName;
                        domainProperty.PropertyContentOverrideValue.MimeType = image.ContentType;
                        if (imageOverrideErrorMessage != null) // process failed for some reason
                        {
                            ViewBag.Message = imageOverrideErrorMessage;
                            /* Let it save the rest without the image override 
                             * so we don't need special mappings just to show the incomplete form/values */
                            //return View(property);// TODO: this will likely fail as mappings are needed to begin edit
                        }
                    }
                }
                else
                {
                   domainProperty.PropertyContentOverride = false;
                   if (domainProperty.PropertyContentOverrideValue != null)
                   {
                       _contentRepository.DeleteContent(domainProperty.PropertyContentOverrideValue.ContentGuid);
                   }
                   domainProperty.PropertyContentOverrideValue = null;
                }

                // === location override ===
                domainProperty.LocationOverride = property.LocationOverride;
                if (property.LocationOverride)
                {
                    domainProperty.LocationOverrideValue = DbGeography.FromText("POINT(" + property.LocationOverrideLng + " " + property.LocationOverrideLat + ")");                 
                }
                // === end location override ===

                // === SiteVisibilityFlags ===
                domainProperty.SiteVisibilityFlagsOverrideValue = DetermineSiteVisibilityFlags(property.SiteVisibilityFlagsToSubmit);  
             
                // === FreeFormFields ===
                var freeFormFieldsToAdd = new List<FreeFormField>();
                // use 'counter' in case they deleted/added fields and SortOrder is non-sequential like 0,1,3,4...
                var counter = 0;
                // clear existing FFF's
                if (domainProperty.FreeFormFields != null)
                {
                    foreach (var fff in domainProperty.FreeFormFields.ToList())
                    {
                        domainProperty.FreeFormFields.Remove(fff);
                        _propertyRepository.Save();
                    }
                }
                // add FFF's from form
                if (property.FreeFormFieldsToSubmit != null)
                {
                    foreach (var pfffavm in property.FreeFormFieldsToSubmit.OrderBy(x => x.SortOrder))
                    {
                        // map to FreeFormField objects
                        var fff = new FreeFormField
                        {
                            FreeFormFieldId = Guid.NewGuid(),
                            FreeFormTemplateId = pfffavm.FreeFormTemplateId,
                            FreeFormGroup = _freeFormGroupRepository
                                .GetFreeFormGroups()
                                .First(x => x.Title == "Property"),
                            SortOrder = counter,
                            StringValue = pfffavm.StringValue,
                            NumberValue = pfffavm.NumberValue,
                            SiteVisibilityFlags = DetermineSiteVisibilityFlags(pfffavm.SiteVisibilityFlagsToSubmit)
                        };
                        freeFormFieldsToAdd.Add(fff);
                        // increment counter
                        counter++;
                    }
                    domainProperty.FreeFormFields = freeFormFieldsToAdd;
                }

                // === Tags overrides
                // handle tag override checkbox
                domainProperty.TagsOverride = property.TagsOverride;
                // delete existing tags
                var tagsToDelete = _tagRepository.GetTags()
                    .Where(x => x.PropertyId == property.PropertyId)
                    .ToList();
                foreach (var tag in tagsToDelete)
                {
                    _tagRepository.DeleteTag(tag.TagId);
                }
                _tagRepository.Save();

                // now add the tags
                var tagList = new List<Tag>();
                if (!string.IsNullOrEmpty(property.TagsOverrideValue) && property.TagsOverride) // catch empty spaces and only insert if checked...
                {
                    // now the comma delimited tag list
                    var tagArray = property.TagsOverrideValue.Trim().Split(',');
                    tagList.AddRange(from t1 in tagArray
                        where t1.Trim().Length > 0
                        select new Tag
                        {
                            TagId = Guid.NewGuid(), 
                            TagName = t1.Trim(), 
                            CreatedDate = DateTime.Now, 
                            Property = domainProperty
                        });
                    // if anything entered besides commas and blanks, add it
                    if (tagList.Count > 0)
                        domainProperty.TagsOverrideValue = tagList;
                }
                // === End Tags ===

                // === contact Override
                domainProperty.CorporateContactOverride = property.CorporateContactOverride;
                domainProperty.CorporateContactOverrideValue = property.CorporateContactOverride ? _userRepository.GetUserProfileByFullName(property.CorporateContactOverrideValue) : null;
                // === end override ===

                // === Leasing Agent Override
                domainProperty.LeasingAgentOverride = property.LeasingAgentOverride;
                domainProperty.LeasingAgentOverrideValue = property.LeasingAgentOverride ? _userRepository.GetUserProfileByFullName(property.LeasingAgentOverrideValue) : null;
                // === end override ===

                // === Bay Options ===
                #region Bay Options
                // TODO: map properties from view model to domain model
                //var BayOptionListFordomainProperty = new List<BayOption>();
                foreach (var ebovm in property.BayOptionsToSubmit)
                {
                    // get BO to update
                    var bo = _bayOptionRepository.GetBayOptionById(ebovm.BayGUID);
                    bo.BayNumberOverride = ebovm.BayNumberOverride;
                    if (ebovm.BayNumberOverride && ebovm.BayNumberOverrideValue != null)
                        bo.BayNumberOverrideValue = ebovm.BayNumberOverrideValue;

                    bo.BaySizeOverride = ebovm.BaySizeOverride;
                    if (ebovm.BaySizeOverride && ebovm.BaySizeOverrideValue > 0)
                        bo.BaySizeOverrideValue = ebovm.BaySizeOverrideValue;

                    bo.BayTypeOverride = ebovm.BayTypeOverride;
                    if (ebovm.DescriptionOverride && ebovm.BayTypeOverrideValue != null)
                        bo.BayTypeOverrideValue = _bayTypeRepository.GetBayTypeByGuid(ebovm.BayTypeOverrideValue);

                    bo.DescriptionOverride = ebovm.DescriptionOverride;
                    if (ebovm.DescriptionOverride && ebovm.DescriptionOverrideValue != null)
                        bo.DescriptionOverrideValue = ebovm.DescriptionOverrideValue;

                    bo.ExcerptOverride = ebovm.ExcerptOverride;
                    if (ebovm.ExcerptOverride && ebovm.ExcerptOverrideValue != null)
                        bo.ExcerptOverrideValue = ebovm.ExcerptOverrideValue;

                    bo.IsFeaturedOverride = ebovm.IsFeaturedOverride;
                    if (ebovm.IsFeaturedOverride)
                        bo.IsFeaturedOverrideValue = ebovm.IsFeaturedOverrideValue;

                    bo.OccupancyStateOverride = ebovm.OccupancyStateOverride;
                    if (ebovm.OccupancyStateOverride && ebovm.OccupancyStateOverrideValue != null)
                        bo.OccupancyStateOverrideValue = (Enums.OccupancyState)Enum.Parse(typeof(Enums.OccupancyState),ebovm.OccupancyStateOverrideValue);

                    // UpdatedSinceLastImport = PriorityTasksController
                    bo.UpdatedSinceImport = true;

                    // Site Visibility Flags
                    bo.SiteVisibilityFlagsOverride = ebovm.SiteVisibilityFlagsOverride;
                    var siteVisibilityTotalValue = 0;
                    if (ebovm.SiteVisibilityFlagsOverrideValue != null)
                    {
                        foreach (var svf in ebovm.SiteVisibilityFlagsOverrideValue)
                        {
                            Enums.SiteVisibilityFlags flagTester;
                            if (Enum.TryParse(svf, out flagTester))
                            {
                                // sum up values to get total and then convert to enum after
                                siteVisibilityTotalValue += (int)flagTester;
                            }
                        }
                        // be sure to account for nothing checked ('1' is 'None', not '0')
                        if (siteVisibilityTotalValue == 0)
                            siteVisibilityTotalValue = 1;
                        // convert siteVisibilityTotalValue to Enum
                        bo.SiteVisibilityFlagsOverrideValue = (Enums.SiteVisibilityFlags)siteVisibilityTotalValue;
                    }

                    // FreeFormFields for Bay Options
                    // clear existing FFF's
                    if (bo.FreeFormFields != null)
                    {
                        foreach (var fff in bo.FreeFormFields.ToList())
                        {
                            bo.FreeFormFields.Remove(fff);
                            _bayOptionRepository.Save();
                        }
                    }
                    var boFFF = new List<FreeFormField>();
                    counter = 0;
                    if (ebovm.FreeFormFieldsToSubmit != null)
                    {
                        foreach (var fffavm in ebovm.FreeFormFieldsToSubmit.OrderBy(x => x.SortOrder))
                        {
                            // map to FreeFormField objects
                            var fff = new FreeFormField
                            {
                                FreeFormFieldId = Guid.NewGuid(),
                                FreeFormTemplateId = fffavm.FreeFormTemplateId,
                                FreeFormGroup = _freeFormGroupRepository
                                    .GetFreeFormGroups()
                                    .First(x => x.Title == "BayOption"),
                                SortOrder = counter,
                                StringValue = fffavm.StringValue,
                                NumberValue = fffavm.NumberValue,
                                SiteVisibilityFlags = DetermineSiteVisibilityFlags(fffavm.SiteVisibilityFlagsToSubmit)
                            };
                            boFFF.Add(fff);
                            // increment counter
                            counter++;
                        }
                        bo.FreeFormFields = boFFF;
                        _bayOptionRepository.Save();
                    }
                }
                // === end Bay Options ===
                #endregion

                // updatedsincelastimport = PriorityTasks controller
                domainProperty.UpdatedSinceImport = true;

                _propertyRepository.Save();
                ViewBag.Message = "Changes Saved.";
                return RedirectToAction("Edit", "Property", new { id = property.PropertyId });
            }
            return View(property);
        }

        private static Enums.SiteVisibilityFlags DetermineSiteVisibilityFlags(string[] siteVisibilityFlags)
        {
            var siteVisibilityTotalValue = 0;
            if (siteVisibilityFlags != null)
            {
                foreach (var svf in siteVisibilityFlags)
                {
                    Enums.SiteVisibilityFlags flagTester;
                    if (Enum.TryParse(svf, out flagTester))
                    {
                        // sum up values to get total and then convert to enum after
                        siteVisibilityTotalValue += (int)flagTester;
                    }
                }
            }
            // be sure to account for nothing checked ('1' is 'None', not '0')
            if (siteVisibilityTotalValue == 0)
                siteVisibilityTotalValue = 1;
            // convert siteVisibilityTotalValue to Enum
            return (Enums.SiteVisibilityFlags)siteVisibilityTotalValue;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _propertyRepository.Dispose();
                _marketRepository.Dispose();
                _freeFormTemplateRepository.Dispose();
                _freeFormGroupRepository.Dispose();
                _tagRepository.Dispose();
                _userRepository.Dispose();
                _contentRepository.Dispose();
                _bayOptionRepository.Dispose();
            }
            base.Dispose(disposing);
        }
       
    }
}
