﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.CMS.ViewModels;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.Controllers
{
    public class ExecutiveSectionController : Controller
    {
        private readonly IExecutiveSectionRepository _executiveSectionRepository;
        public ExecutiveSectionController(
            IExecutiveSectionRepository executiveSectionRepository
            )
        {
            _executiveSectionRepository = executiveSectionRepository;
        }
 
        // GET: ExecutiveSection
        public ActionResult Index()
        {
            return View(_executiveSectionRepository.GetExecutiveSections()
                .OrderBy(x => x.SortOrder).ToList());
        }
        
        // GET: ExecutiveSection/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var executiveSection = _executiveSectionRepository
                .GetExecutiveSectionById(idInt);
            if (executiveSection == null)
            {
                return HttpNotFound();
            }
            return View(executiveSection);
        }

        // GET: ExecutiveSection/Create
        public ActionResult Create()
        {
            var viewModel = new CreateExecutiveSectionViewModel
            {
                ExecutiveSectionCount = _executiveSectionRepository
                    .GetExecutiveSections()
                    .Count()
            };
            return View(viewModel);
        }

        // POST: ExecutiveSection/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ExecutiveSectionId,SectionName,SectionBlurb,SortOrder")] ExecutiveSection executiveSection)
        public ActionResult Create(CreateExecutiveSectionViewModel executiveSection)
        {
            // catch/check ModelState errs
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            if (ModelState.IsValid)
            {
                // map from viewModel to domainModel
                ExecutiveSection domainModel = new ExecutiveSection();
                domainModel.SectionName = executiveSection.SectionName;
                domainModel.SectionBlurb = executiveSection.SectionBlurb;
                domainModel.SortOrder = executiveSection.SortOrder;
                _executiveSectionRepository.InsertExecutiveSection(domainModel);
                // shuffle sortOrder
                var esList = _executiveSectionRepository.GetExecutiveSections()
                    .ToList();
                foreach (var es in esList.Where(es => es.SortOrder >= executiveSection.SortOrder))
                {
                    // increment sortOrder
                    es.SortOrder++;
                }
                _executiveSectionRepository.Save();
                return RedirectToAction("Index");
            }

            return View(executiveSection);
        }

        // GET: ExecutiveSection/Edit/5
        public ActionResult Edit(int id)
        {
            var executiveSection = _executiveSectionRepository.GetExecutiveSectionById(id);
            if (executiveSection == null)
            {
                return HttpNotFound();
            }
            var viewModel = new EditExecutiveSectionViewModel
            {
                ExecutiveSectionId = id,
                ExecutiveSectionCount = _executiveSectionRepository
                                                    .GetExecutiveSections()
                                                    .Count(),
                SectionBlurb = executiveSection.SectionBlurb,
                SectionName = executiveSection.SectionName,
                SortOrder = executiveSection.SortOrder
            };
            return View(viewModel);
        }

        // POST: ExecutiveSection/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ExecutiveSectionId,SectionName,SectionBlurb,SortOrder")] ExecutiveSection executiveSection)
        public ActionResult Edit(EditExecutiveSectionViewModel executiveSection)
        {
            if (ModelState.IsValid)
            {
                // Keep track of OLD sortOrder
                var oldEs = _executiveSectionRepository.GetExecutiveSectionById(executiveSection.ExecutiveSectionId);
                var oldSortOrder = oldEs.SortOrder;
                // map viewmodel to domainModel
                var domainModel = _executiveSectionRepository.GetExecutiveSectionById(executiveSection.ExecutiveSectionId);
                domainModel.ExecutiveSectionId = executiveSection.ExecutiveSectionId;
                domainModel.SectionName = executiveSection.SectionName;
                domainModel.SectionBlurb = executiveSection.SectionBlurb;
                domainModel.SortOrder = executiveSection.SortOrder;
                var executiveSections = _executiveSectionRepository.GetExecutiveSections()
                    .Where(x => x.ExecutiveSectionId != executiveSection.ExecutiveSectionId).ToList();
                foreach (var esToModify in executiveSections)
                {
                    // SHUFFLE: if the new sortorder is greater then the old sortorder
                    if (esToModify.SortOrder > oldSortOrder)
                    {
                        if (esToModify.SortOrder > oldSortOrder && esToModify.SortOrder <= executiveSection.SortOrder)
                            esToModify.SortOrder--;
                    }
                    else
                    {
                        if (esToModify.SortOrder <= oldSortOrder && esToModify.SortOrder >= executiveSection.SortOrder)
                            esToModify.SortOrder++;
                    }
                }
                //db.Entry(executiveSection).State = EntityState.Modified;
                _executiveSectionRepository.Save();
                return RedirectToAction("Index");
            }
            return View(executiveSection);
        }

        // GET: ExecutiveSection/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var executiveSection = _executiveSectionRepository.GetExecutiveSectionById(idInt);
            if (executiveSection == null)
            {
                return HttpNotFound();
            }
            return View(executiveSection);
        }

        // POST: ExecutiveSection/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _executiveSectionRepository.DeleteExecutiveSection(id);
            _executiveSectionRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _executiveSectionRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
