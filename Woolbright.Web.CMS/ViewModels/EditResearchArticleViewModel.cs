﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class EditResearchArticleViewModel
    {
        [Key]
        public int ArticleId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string ShortDescription { get; set; }

        [DisplayName("Body Folder")]
        public List<string> BodyFolderChoices { get; set; }
        
        public string BodyUrl { get; set; }

        public string BodyPathLocal { get; set; }

        public DateTime DatePublished { get; set; }

        public int OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public virtual UserProfile Owner { get; set; }

        // content
        public string ThumbnailAzureReference { get; set; }

        // tags
        public ICollection<Tag> Tags { get; set; }
        public string TagsOverrideValue { get; set; }

    }
}