﻿using System;

namespace Woolbright.Web.CMS.ViewModels
{
    public class PriorityTasksBayOptionsViewModel
    {
        public Guid BayGuid { get; set; }
        public string BayNumber { get; set; }
        public int PropertyId { get; set; }

    }
}