﻿using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class ImageFromModelByteArrayViewModel
    {
        public string ImgSrcTagName { get; set; }

        public string ImgWidth { get; set; }

        public string ImgHeight { get; set; }
      
        public Content Content { get; set; }
    }
}