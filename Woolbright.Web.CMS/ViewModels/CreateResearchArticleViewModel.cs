﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class CreateResearchArticleViewModel
    {
        [DataType(DataType.MultilineText)]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string ShortDescription { get; set; }

        //[DataType(DataType.MultilineText)]
        //public string BodyFolder { get; set; }

        [DisplayName("Body Folder")]
        public string BodyFolderChoices { get; set; }

        public virtual Content Thumbnail { get; set; }

        public DateTime PublishDate { get; set; }

        public ICollection<Tag> Tags { get; set; }
        public string TagsOverrideValue { get; set; }
     
    }
}