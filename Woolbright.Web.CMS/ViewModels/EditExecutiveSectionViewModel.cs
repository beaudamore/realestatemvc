﻿
namespace Woolbright.Web.CMS.ViewModels
{
    public class EditExecutiveSectionViewModel
    {
        // for loading
        public int ExecutiveSectionCount { get; set; }
        // for posting
        public string SectionName { get; set; }
        public string SectionBlurb { get; set; }
        public int SortOrder { get; set; }
        // for both
        public int ExecutiveSectionId { get; set; }
    }
}