﻿
namespace Woolbright.Web.CMS.ViewModels
{
    public class CreateExecutiveSectionViewModel
    {
        // for loading
        public int ExecutiveSectionCount { get; set; }
        // for posting
        public string SectionName { get; set; }
        public string SectionBlurb { get; set; }
        public int SortOrder { get; set; }

        //public virtual ICollection<ExecutiveSectionMapping> ExecutiveSectionMappings { get; set; }
        
    }
}