﻿using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class ArchiveResearchArticleViewModel
    {
        public Dictionary<string, int> ArchiveList { get; set; }
        public ICollection<ResearchArticle> Articles { get; set; }

    }
}