﻿using System.ComponentModel.DataAnnotations;

namespace Woolbright.Web.CMS.ViewModels
{
    public class ExecutiveSectionChoicesViewModel
    {
        public int ExecutiveSectionId { get; set; }
        [UIHint("YesNo")]
        public bool IsSelected { get; set; }
        public int SortOrder { get; set; }

    }
}