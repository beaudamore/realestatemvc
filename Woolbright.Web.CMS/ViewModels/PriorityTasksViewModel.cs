﻿using System.Collections.Generic;

namespace Woolbright.Web.CMS.ViewModels
{
    public class PriorityTasksViewModel
    {
        public List<PriorityTasksPropertiesViewModel> Properties { get; set; }
        //public List<PriorityTasksBayOptionsViewModel> BayOptions { get; set; }

    }
}