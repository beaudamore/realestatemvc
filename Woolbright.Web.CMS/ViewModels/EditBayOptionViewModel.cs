﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class EditBayOptionViewModel
    {
        public Guid BayGUID { get; set; } //(pseudo-key)

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool ExcerptOverride { get; set; }
        [DataType(DataType.MultilineText)]
        public string ExcerptOverrideValue { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool DescriptionOverride { get; set; }
        [DataType(DataType.MultilineText)]
        public string DescriptionOverrideValue { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool IsFeaturedOverride { get; set; }
        public bool IsFeaturedOverrideValue { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool BayNumberOverride { get; set; }
        public string BayNumberOverrideValue { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool BaySizeOverride { get; set; } // comes from poems and can be overridden
        public int BaySizeOverrideValue { get; set; } // comes from poems and can be overridden

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool BayTypeOverride { get; set; }
        public Guid BayTypeOverrideValue { get; set; } // comes from poems and can be overridden

      
        //[DisplayName("Occupancy State")]
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool OccupancyStateOverride { get; set; } // comes from poems and can be overridden
        public string OccupancyStateOverrideValue { get; set; } // comes from poems and can be overridden
        //[DisplayName("Occupancy State Override")]
       
        // Site visibility
        // for form
        //public Enums.SiteVisibilityFlags AvailableSiteVisibilityFlags { get; set; }
        // for posting
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool SiteVisibilityFlagsOverride { get; set; }
        public string[] SiteVisibilityFlagsOverrideValue { get; set; }

        public ICollection<FreeFormField> FreeFormFieldsForThisBayOption { get; set; } 
        public ICollection<FreeFormFieldAddViewModel> FreeFormFieldsToSubmit { get; set; }
       
    }
}