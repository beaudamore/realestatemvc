﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class PropertyDetailsViewModel
    {
        [Key]
        public int PropertyId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public DbGeography Location { get; set; }
        [UIHint("YesNo")]
        public bool LocationOverride { get; set; }
        public DbGeography LocationOverrideValue { get; set; }

        public string WebsiteUrl { get; set; }

        public string Description { get; set; }

        public int SquareFootage { get; set; }

        public int GrossLeasableArea { get; set; }

        public int MinDivisible { get; set; }

        public int MaxContiguous { get; set; }

        public ICollection<FreeFormField> FreeFormFields { get; set; }
        
        public ICollection<Tag> Tags { get; set; }

        public int CorporateContactId { get; set; }
        [ForeignKey("CorporateContactId")]
        public virtual UserProfile CorporateContact { get; set; }

        public int LeasingAgentId { get; set; }
        [ForeignKey("LeasingAgentId")]
        public virtual UserProfile LeasingAgent { get; set; }

        public int MarketId { get; set; }
        [ForeignKey("MarketId")]
        public virtual Market Market { get; set; }

        public int PropertyContentId { get; set; }
        [ForeignKey("PropertyContentId")]
        public virtual Content PropertyContent { get; set; }

        //[UIHint("YesNo")]
        [DisplayName("Site Visibility")]
        public Enums.SiteVisibilityFlags SiteVisibilityFlags { get; set; }      

        [InverseProperty("Property")]
        public virtual ICollection<BayOption> BayOptions { get; set; }
    }
}