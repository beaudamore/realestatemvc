﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class CreateExecutiveViewModel
    {
       
        // exec specific
        public int UserProfileId { get; set; }

        [DataType(DataType.MultilineText)]
        public string ExecutiveTitle { get; set; }

        [DataType(DataType.MultilineText)]
        public string ExecutiveBlurb { get; set; }
        public List<Executive> Executives { get; set; }

        // main picture
        public int ProfilePictureContentId { get; set; }
        [DisplayName("Main Pic")]
        [ForeignKey("ProfilePictureContentId")]
        public virtual Content ProfilePictureContent { get; set; }

        // black and white / alternate pic
        public int ProfileSidePictureContentId { get; set; }
        [DisplayName("Alt Pic")]
        [ForeignKey("ProfileSidePictureContentId")]
        public virtual Content ProfileSidePictureContent { get; set; }

        // the sections to show this executive in
        //[InverseProperty("Executives")]
        public ICollection<ExecutiveSection> ExecutiveSections { get; set; }
        public ExecutiveSectionChoicesViewModel[] ExecutiveSectionChoices { get; set; }


    }
}