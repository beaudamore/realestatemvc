﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class EditExecutiveViewModel
    {
       
        // exec specific
        public int ExecutiveId { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string ExecutiveTitle { get; set; }
        public string ExecutiveBlurb { get; set; }
        public List<Executive> Executives { get; set; }

        // main picture
        public int ProfilePictureContentId { get; set; }
        [ForeignKey("ProfilePictureContentId")]
        public virtual Content ProfilePictureContent { get; set; }

        // black and white / alternate pic
        public int ProfileSidePictureContentId { get; set; }
        [ForeignKey("ProfileSidePictureContentId")]
        public virtual Content ProfileSidePictureContent { get; set; }

        // the sections to show this executive in
        //[InverseProperty("Executives")]
        public ICollection<ExecutiveSection> ExecutiveSections { get; set; }
        public ICollection<ExecutiveSectionMapping> ExecutiveSectionMappings { get; set; }
        public ExecutiveSectionChoicesViewModel[] ExecutiveSectionChoices { get; set; }


    }
}