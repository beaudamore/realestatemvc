﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class EditPropertyViewModel
    {
        public EditPropertyViewModel()
        {
            //Markets = new List<SelectListItem>();
        }

        [Key]
        public int PropertyId { get; set; }

        //[Required]
        public string Name { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool NameOverride { get; set; }
        public string NameOverrideValue { get; set; }
       
        //[Required]
        public string Address { get; set; }
        
        public string Address2 { get; set; }
       
        //[Required]
        public string City { get; set; }
        
        //[Required]
        public string State { get; set; }

        //[Required(ErrorMessage = "Zip is a Required field.")]
        //[DisplayName("Zip (xxxxx)")]
        //[DataType(DataType.Text)]
        //[RegularExpression("\\d{5}", ErrorMessage = "Zip Code is required and must be properly formatted. (xxxxx)")]
        public string Zip { get; set; }
        
        // Location and override
        public DbGeography Location { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool LocationOverride { get; set; }
        public DbGeography LocationOverrideValue { get; set; }
        [DisplayName("Lat Override")]
        public float LocationOverrideLat { get; set; }
        [DisplayName("Lng Override")]
        public float LocationOverrideLng { get; set; }
       
        //[Required]
        public string Description { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool DescriptionOverride { get; set; }
        [DataType(DataType.MultilineText)]
        public string DescriptionOverrideValue { get; set; }
                
        public int SquareFootage { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool SquareFootageOverride { get; set; }
        public int SquareFootageOverrideValue { get; set; }
   
        public int GrossLeasableArea { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool GrossLeasableAreaOverride { get; set; }
        public int GrossLeasableAreaOverrideValue { get; set; }
   
        public int MinDivisible { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool MinDivisibleOverride { get; set; }
        public int MinDivisibleOverrideValue { get; set; }
    
        public int MaxContiguous { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool MaxContiguousOverride { get; set; }
        public int MaxContiguousOverrideValue { get; set; }
 
        public string WebsiteUrl { get; set; }
       
        // FreeForm stuff
        public ICollection<FreeFormField> FreeFormFieldsForThisProperty { get; set; }
        public ICollection<FreeFormFieldAddViewModel> FreeFormFieldsToSubmit { get; set; }
        // Existing FreeFormfields
        public ICollection<FreeFormTemplate> FreeFormTemplates { get; set; }
        // for the jQuery 'add new template' functionality
        public ICollection<Enums.FreeFormValueType> FreeFormValueTypes { get; set; }    
        // End 'FreeForm' stuff
        
        // for form
        //public ICollection<User> CorporateContacts { get; set; }
        public Dictionary<string, string> CorporateContacts { get; set; }

        // for post - contact/agent
        [DisplayName("Corporate Contact")]
        public string CorporateContactFullName { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool CorporateContactOverride { get; set; }
        public string CorporateContactOverrideValue { get; set; } // this is 'Full Name' of UserProfile

        [DisplayName("Leasing Agent")]
        public string LeasingAgentFullName { get; set; }

        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool LeasingAgentOverride { get; set; }
        public string LeasingAgentOverrideValue { get; set; } // this is 'Full Name' of UserProfile

        // for autocomplete to choose Corp contact and leasing agent overrides
        public ICollection<string> UserProfileFullNames { get; set; }
        // --------------------

        // for form
        public ICollection<Market> Markets { get; set; }
        // for post
        [Required]
        [DisplayName("Market")]
        public int MarketId { get; set; }

        public int PropertyContentId { get; set; }
        [ForeignKey("PropertyContentId")]
        public Content PropertyContent { get; set; }
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool PropertyContentOverride { get; set; }
        [DisplayName("Content Override")]
        public Content PropertyContentOverrideValue { get; set; }

        // TAGS
        // for Form
        public ICollection<Tag> TagsExisting { get; set; }
        public ICollection<Tag> Tags { get; set; }
        // Tags for posting
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool TagsOverride { get; set; }
        public string TagsOverrideValue { get; set; }

        // Site visibility
        // for form
        public Enums.SiteVisibilityFlags SiteVisibilityFlagsExisting { get; set; }
        public Enums.SiteVisibilityFlags SiteVisibilityFlagsOverrideValue { get; set; }
        // for posting
        [DisplayName("Override")]
        [UIHint("YesNo")]
        public bool SiteVisibilityFlagsOverride { get; set; }
        public string[] SiteVisibilityFlagsToSubmit { get; set; }
       
        // === BayOptions ===
        // For form
        public virtual ICollection<BayOption> BayOptions { get; set; }
        // for posting 
        public ICollection<EditBayOptionViewModel> BayOptionsToSubmit { get; set; }
        public Dictionary<string,string> BayTypes { get; set; }
     

    }
}