﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.CMS.ViewModels
{
    public class FreeFormFieldAddViewModel
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public Guid FreeFormFieldId { get; set; }

        [Required]
        public Guid FreeFormTemplateId { get; set; }
        [ForeignKey("FreeFormTemplateId ")]
        public virtual FreeFormTemplate FreeFormTemplate { get; set; }

        //public string LabelOverride { get; set; }

        public string StringValue { get; set; }
        public int NumberValue { get; set; }
        //public DbGeography Location { get; set; }
        //public virtual Content Content { get; set; }

        public int SortOrder { get; set; }

        public string[] SiteVisibilityFlagsToSubmit { get; set; }
       

    }
}