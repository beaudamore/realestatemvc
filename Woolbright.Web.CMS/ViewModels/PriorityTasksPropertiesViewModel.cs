﻿
namespace Woolbright.Web.CMS.ViewModels
{
    public class PriorityTasksPropertiesViewModel
    {
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }

    }
}