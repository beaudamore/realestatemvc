﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Woolbright.Web.Data.Classes;

namespace Woolbright.Web.CMS.ViewModels
{
    public class CreateBayOptionViewModel
    {     
        public string Excerpt { get; set; }
        public string Description { get; set; }
        public bool IsFeatured { get; set; }

        [Required]
        [DisplayName("Bay Number")]
        public string BayNumber { get; set; }

        [Required]
        [DisplayName("Bay Size")]
        public string BaySize { get; set; } // comes from poems and can be overridden
        //[DisplayName("Bay Size Override")]
        //public int? BaySizeOverride { get; set; }

        [DisplayName("Bay Type")]
        public string BayType { get; set; } // comes from poems and can be overridden
        //[DisplayName("Bay Type Override")]
        //public int? BayTypeOverride { get; set; }

        //[DisplayName("Occupancy State")]
        //public int OccupancyState { get; set; } // comes from poems and can be overridden
        //[DisplayName("Occupancy State Override")]
        //public int? OccupancyStateOverride { get; set; } // comes from poems and can be overridden
        
        // Site visibility
        // for form
        public Enums.SiteVisibilityFlags AvailableSiteVisibilityFlags { get; set; }
        // for posting
        public string[] SiteVisibilityFlags { get; set; }
        
        public int PropertyId { get; set; }
        public string PropertyNameAndCityAndState { get; set; }

    }
}