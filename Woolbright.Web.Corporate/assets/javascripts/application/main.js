/*============================
=            Main            =
============================*/

;(function ( $ ) {
  'use strict';

  var IS_MOBILE = ( navigator.userAgent.match(/Android/i) ||
                    navigator.userAgent.match(/webOS/i) ||
                    navigator.userAgent.match(/iPhone/i) ||
                    navigator.userAgent.match(/iPod/i) ||
                    navigator.userAgent.match(/BlackBerry/i) ||
                    navigator.userAgent.match(/Windows Phone/i) ||
                    navigator.userAgent.match(/iPad/i) );
  var click;

  $( window ).on( 'load', function() {
    $( 'body' ).addClass( 'ready' );
  });

  var wow = new WOW({
      boxClass:     'reveal',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       false,       // default
      live:         true        // default
    }
  )
  wow.init();

  $( 'select' ).chosen({
    disable_search_threshold: 10
  });

  if( IS_MOBILE ) {
    click = 'touchend';
  } else {
    click = 'mouseup';
  }

  // show search on desktop
  if( document.getElementById( 'search-trigger-desktop' ) ) {
    document.getElementById( 'search-trigger-desktop' ).addEventListener('click', function( event ) {
      event.preventDefault();
      document.querySelector('.search__box.desktop').classList.add( 'searching' );
    });
  }

  // show search on mobile
  if( document.getElementById( 'search-trigger-mobile' ) ) {
    document.getElementById( 'search-trigger-mobile' ).addEventListener('click', function( event ) {
      event.preventDefault();
      document.querySelector('.search__box.mobile').classList.add( 'searching' );
    });
  }

  // hide search on scroll
  window.addEventListener( 'scroll', function( event ) {
    document.querySelector('.search__box.desktop').classList.remove( 'searching' );
    document.querySelector('.search__box.mobile').classList.remove( 'searching' );
  });

  // hide search when clicking anywhere
  $(document).on( click, function (e) {
    var container = $('.search__box');

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass( 'searching' );
    }
  });

  // show map
  if( document.getElementById('open-map') ) {
    document.getElementById('open-map').addEventListener( 'click', function( event ) {
      event.preventDefault();
      document.querySelector('.site-section__portfolio--map-container').classList.add('show-map');
    });
  }

  // hide map
  if( document.getElementById('close-map') ) {
    document.getElementById('close-map').addEventListener( 'click', function( event ) {
      event.preventDefault();
      document.querySelector('.site-section__portfolio--map-container').classList.remove('show-map');
    });
  }

})( jQuery );

/*-----  End of Main  ------*/



