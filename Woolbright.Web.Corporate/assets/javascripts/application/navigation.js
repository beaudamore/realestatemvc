/*==================================
=            Navigation            =
==================================*/

// setup global breakpoints to match Sass variables/mixins
var breakpoint_mobile = 999;
var breakpoint_phone = 767;


// create foreach method
var forEach = function (array, callback, scope) {
  for (var i = 0; i < array.length; i++) {
    callback.call(scope, i, array[i]);
  }
};

;(function ( $ ) {
  'use strict';

  // initialize headroom
  $( '.site-header' ).headroom({
    offset : 0
  });

})( jQuery );


/*==========  Custom Navigation  ==========*/

(function() {

  // create variables
  var path_array = window.location.pathname.split('/');
  var main_menu = document.querySelector('.site-navigation');

  var level_one = document.querySelector('.site-menu');
  var level_two = document.querySelector('.site-menu__secondary');
  var current_level_one = '/' + path_array[1];
  var current_level_two = '/' + path_array[2];

  var level_two_container = document.querySelector('.site-navigation__secondary');
  var level_two_parent = level_two.parentNode.children[1];
  var level_two_origin = level_two.parentNode;

  // menu constructor function
  function Menu_Level(level, current) {
    this.level = level;
    this.items = this.level.children;
    this.current = current;

    level = this.level;
    items = this.items;
    current = this.current;

    forEach(items, function (i, value) {
      value.classList.remove('active');
      if( value.children[0].href + '/' === window.location.href || value.children[0].href === window.location.href ) {
        value.classList.add('current-page');
      }
      if( value.children[0].pathname === current ) {
        if( !value.classList.contains('search') ) {
          value.classList.add('active');
        }
        if( level_two_origin.classList.contains('has-children') && level_two_origin.classList.contains('active') ) {
          level_two_container.classList.add('show');
        }
      } else if( value.children[0].pathname === current_level_one + current ) {
        value.classList.add('active');
      } else if ( value.children[0].pathname === current_level_one + current_level_two + current ) {
        value.classList.add('active');
      } else if ( current_level_one === '/' || current_level_one === '/index.html' ) {
        items[0].classList.add('active');
      }
    });
  }

  // menu toggle button
  document.querySelector('.open-mobile-menu').addEventListener('click', function( event ){
    event.preventDefault();
    if( main_menu.classList.contains('show') ) {
      main_menu.classList.remove('show');
      document.body.classList.remove('show-menu');
    } else {
      main_menu.classList.add('show');
      document.body.classList.add('show-menu');
    }
  });
  document.querySelector('.close-mobile-menu').addEventListener('click', function( event ){
    event.preventDefault();
    if( main_menu.classList.contains('show') ) {
      main_menu.classList.remove('show');
      document.body.classList.remove('show-menu');
    } else {
      main_menu.classList.add('show');
      document.body.classList.add('show-menu');
    }
  });

  // setup menu levels
  new Menu_Level(level_one, current_level_one);
  new Menu_Level(level_two, current_level_two);

  // move menu from top list to secondary navigation area
  level_two.classList.remove('no-desktop');
  level_two_container.appendChild(level_two);

  window.addEventListener('resize', function() {
    main_menu.classList.remove('show');
    document.body.classList.remove('show-menu');
  });

})();

/*-----  End of Navigation  ------*/



