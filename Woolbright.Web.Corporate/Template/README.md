Woolbright Static
=================

## How to Use

With Node.js and Grunt installed, run `npm install` and `bower install` from inside the project directory. This will install the project dependencies. Once completed, run `grunt` and visit http://localhost:9001/ in your browser to view the project. With grunt running you can make changes to the files in the assets directory and they will be automatically compiled.

You can edit project preferences inside of `Gruntfile.js`
