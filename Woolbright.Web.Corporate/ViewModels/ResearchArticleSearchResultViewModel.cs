﻿using System;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class ResearchArticleSearchResultViewModel
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public DateTime PublishDate { get; set; }
       
    }
}