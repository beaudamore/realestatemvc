﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class ResearchArticleViewModel
    {
        public Dictionary<string, string> ArchiveList { get; set; }
        public string ArticleTitle { get; set; }
        public string ArticleShortDescription { get; set; }
        public MvcHtmlString ArticleBodyIndexHtmlString { get; set; }
        public string ArticleBodyIndexHtmlAbsolutePath { get; set; }
        public string ArticleBodyIndexHtmlRelativePath { get; set; }
 
    }
}