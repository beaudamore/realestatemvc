﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class ExecutiveIndexViewModel
    {       
        // exec specific
        public int UserId { get; set; }
        public UserProfile User { get; set; }

        public string ExecutiveTitle { get; set; }
        public string ExecutiveBlurb { get; set; }

        // user
        public int ExecutiveId { get; set; }
        [ForeignKey("ExecutiveId")]
        public Executive Executive { get; set; }

        // main picture
        public int ProfilePictureContentId { get; set; }
        [ForeignKey("ProfilePictureContentId")]
        public virtual Content ProfilePictureContent { get; set; }

        // black and white / alternate pic
        public int ProfileSidePictureContentId { get; set; }
        [ForeignKey("ProfileSidePictureContentId")]
        public virtual Content ProfileSidePictureContent { get; set; }

        // the sections to show this executive in
        //[InverseProperty("Executives")]
        public virtual ICollection<ExecutiveSection> ExecutiveSections { get; set; }  

    }
}