﻿using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class ResearchArticleListViewModel
    {
        public Dictionary<string, string> ArchiveList { get; set; }
        public ICollection<ResearchArticle> Articles { get; set; }
    }
}