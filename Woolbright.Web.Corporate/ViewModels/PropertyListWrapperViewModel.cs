﻿using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class PropertyListWrapperViewModel
    {
        public string RouteIdValue { get; set; }

        public List<Market> Regions { get; set; }
    
        public List<PropertyListViewModel> PLVMs { get; set; }
    }
}