﻿using System.Collections.Generic;
namespace Woolbright.Web.Corporate.ViewModels
{
    public class SendEmailViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public string PropertyId { get; set; }
    
   
    }
}