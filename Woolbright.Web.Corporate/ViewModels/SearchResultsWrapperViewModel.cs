﻿using System.Collections.Generic;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.ViewModels
{
    public class SearchResultsWrapperViewModel
    {
        public string SearchValue { get; set; }

        public List<Property> Properties { get; set; }

        public List<ResearchArticle> ResearchArticles { get; set; }


    }
}