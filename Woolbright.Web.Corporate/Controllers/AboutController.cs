﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Corporate.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Corporate.Controllers
{    
    public class AboutController : Controller
    {
        private readonly IExecutiveRepository _executiveRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly IExecutiveSectionRepository _executiveSectionRepository;
        private readonly IResearchArticleRepository _researchArticleRepository;
        private readonly IContentRepository _contentRepository;
     
        public AboutController(
            IExecutiveRepository executiveRepository, 
            IUserProfileRepository userRepository, 
            IExecutiveSectionRepository executiveSectionRepository,
            IResearchArticleRepository researchArticleRepository, 
            IContentRepository contentRepository
            )
         {
            _executiveRepository = executiveRepository;
            _userRepository = userRepository;
            _executiveSectionRepository = executiveSectionRepository;
            _researchArticleRepository = researchArticleRepository;
            _contentRepository = contentRepository;
       }       
        
        // GET: About
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult AboutUs()
        //{
        //    return View();
        //}

        //[ActionName("Corporate-Profile")]
        public ActionResult CorporateProfile()
        {
            return View();
        }

        //[ActionName("Job-Opportunities")]
        public ActionResult JobOpportunities()
        {
            return View();
        }

        // method to handle sections
        [HttpGet]
        //[ActionName("Executive-Profile")]
        public ActionResult ExecutiveProfile()
        {
            // bring back sections with executives ordered per section
            var model = _executiveSectionRepository.GetExecutiveSections()
                .OrderBy(x => x.SortOrder);
            // order mappings/executives
            foreach (var section in model)
            {
                section.ExecutiveSectionMappings = section.ExecutiveSectionMappings.OrderBy(x => x.SortOrder).ToList();
            }
            return View(model);
        }
        // method to handle sections
        [HttpGet]
        //[ActionName("Executive-Details")]
        public ActionResult ExecutiveDetails(int id)
        {
            var model = _executiveRepository.GetExecutiveById(id);
            return View(model);
        }

        // GET: ResearchArticle
        [HttpGet]
        //[ActionName("Research-Article")]
        public ActionResult ResearchArticle(string id) // if 'id' is passed it's coming from the Search Results
        {
            // viewmodel
            var viewModel = new ResearchArticleListViewModel
            {
                ArchiveList = GetArchiveList()
            };
            // for archive DDL - changed from date/count to date/date where one has 'july' instead of 7
            ViewBag.ArchiveList = new SelectList(viewModel.ArchiveList, "Key", "Value", null);

            // get archives or specific article
            if (!string.IsNullOrEmpty(id))
            {
                var newId = Convert.ToInt32(id);
                viewModel.Articles = _researchArticleRepository
                    .GetResearchArticles()
                    .Where(x => x.ArticleId == newId)
                    .ToList();
                // meta tags
                var article = viewModel.Articles
                    .FirstOrDefault();
                if (article != null)
                {
                    ViewBag.Title = article.Title;
                    ViewBag.Description = article.ShortDescription;
                    ViewBag.Keywords = article.Tags;
                }
            }
            else
            {
                // current monthly articles
                viewModel.Articles = _researchArticleRepository.GetResearchArticles()
                    .Where(
                        x =>
                            x.DatePublished.Year == DateTime.Today.Year
                            && x.DatePublished.Month == DateTime.Today.Month)
                    .ToList(); // this month only     
            }

            return View(viewModel);
        }
        //[ActionName("Research-Archive")]
        public ActionResult ResearchArchive(string id)// this is in format 'mm-yyyy'
        {
            // viewmodel to return
            var viewModel = new ResearchArticleListViewModel();
            // parse date sent
            var postedDateInfo = id.Split('-');
            var month = int.Parse(postedDateInfo[0]);
            var year = int.Parse(postedDateInfo[1]);
            // get articles
            viewModel.Articles = _researchArticleRepository.GetResearchArticles()
                .Where(x => x.DatePublished.Month == month && x.DatePublished.Year == year)
                .ToList();
            // get archives
            viewModel.ArchiveList = GetArchiveList();
            // for archive ddl
            ViewBag.ArchiveListBag = new SelectList(viewModel.ArchiveList, "Key", "Value", id);

            return View("ResearchArticle", viewModel);
        }

        // GET: ResearchArticle/Details/5
        //[ActionName("Research-Article-Details")]
        public ActionResult ResearchArticleDetails(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // parse title-on-date sent
            var articleLinkText = id.Split(new string[] { "-on-" }, StringSplitOptions.None);
            var title = articleLinkText[0].Replace("-", " ");
            var datePublished = DateTime.Parse(articleLinkText[1]);

            var researchArticle = _researchArticleRepository
                .GetResearchArticles()
                .FirstOrDefault(x => x.Title == title && x.DatePublished == datePublished);
            if (researchArticle == null)
            {
                return HttpNotFound();
            }

            // === Get body content from Azure blobs ===
            // BASE DIRECTORY
            var baseDirectory = ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"];
            Azure.CreateFolderAndSetPermissionsIfNotExists(baseDirectory);

            // CONTAINER SPECIFIC
            var localdirectory = baseDirectory + researchArticle.BodyRelativeLocalPath;
            Azure.CreateFolderAndSetPermissionsIfNotExists(localdirectory);

            // 2. check/compare lastModifiedDate and pull blobs if necessary
            Azure.GetIndexHtmlFromAzureIfNewerByContainerName(researchArticle);

            // 3. show content via getting HTML from 'index' page into Model
            var ravm = new ResearchArticleViewModel();
            ravm.ArticleTitle = researchArticle.Title;
            ravm.ArticleShortDescription = researchArticle.ShortDescription;
            ravm.ArticleBodyIndexHtmlRelativePath = ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] + researchArticle.BodyRelativeLocalPath + "/index.html";
            //ravm.ArticleBodyIndexHtmlAbsolutePath = Server.MapPath(ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] + researchArticle.BodyRelativeLocalPath + "/index.html");
            //ravm.ArticleBodyIndexHtmlString = MvcHtmlString.Create(System.IO.File.ReadAllText(Server.MapPath(ConfigurationManager.AppSettings["ResearchArticleBodyRelativeLocalDirectory"] + researchArticle.BodyRelativeLocalPath + "/index.html")));
            
            // SEO
            ViewBag.Title = researchArticle.Title;
            ViewBag.Description = researchArticle.ShortDescription;
            ViewBag.Keywords = ContentSpecific.GetCommaDelimNameStringFromTags(researchArticle.Tags.ToList());

            return View(ravm);
        }

        [ChildActionOnly]
        public ActionResult GetHtmlPage(string path)
        {
            return new FilePathResult(path, "text/html");
        }

        [ChildActionOnly]
        public ActionResult GetHtml(string path)
        {
            return File(Server.MapPath(path), "text/html");
        }
        
        private Dictionary<string, string> GetArchiveList()
        {
            //var lastDayOfLastMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);
            var grouped = (from p in _researchArticleRepository.GetResearchArticles()
                           group p by new { month = p.DatePublished.Month, year = p.DatePublished.Year } into d
                           select new
                           {
                               dt = d.Key.month + "-" + d.Key.year,
                               dtByMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(d.Key.month) + " " + d.Key.year
                           }//count = d.Count() }
                           )
                           .OrderByDescending(g => g.dt);
            return grouped.ToDictionary(item => item.dt, item => item.dtByMonthName);
            // pre-resharper
            //foreach (var item in grouped)
            //{
            //    archiveList.Add(item.dt, item.count);
            //}
            //return archiveList;
        }
    }
}
