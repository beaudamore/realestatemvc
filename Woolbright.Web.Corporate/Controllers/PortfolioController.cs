﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Woolbright.Web.Corporate.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.Controllers
{
    public class PortfolioController : Controller
    {       
        private readonly IPropertyRepository _propertyRepository;
        private readonly IMarketRepository _marketRepository;
      
        public PortfolioController(
            IPropertyRepository propertyRepository, 
            IMarketRepository marketRepository
            )
        {
            _propertyRepository = propertyRepository;
            _marketRepository = marketRepository;
        }
       
        // GET: Property
        public ActionResult Index()
        {
            var propList = _propertyRepository.GetProperties()
                .Where(x => x.SiteVisibilityFlags.HasFlag(Enums.SiteVisibilityFlags.Corporate)
                            || (x.SiteVisibilityFlagsOverride
                                && x.SiteVisibilityFlagsOverrideValue.HasFlag(Enums.SiteVisibilityFlags.Corporate))
                            && x.UpdatedSinceImport
                                )// just ones visible to 'Corporate'
                .OrderBy(x => x.Name)
                .Select(y => new PropertyListViewModel { Name = y.Name, City = y.City ?? string.Empty })
                .ToList();
            var regions = _marketRepository.GetMarkets()
                .OrderBy(x => x.SortOrder)
                .ToList();
            // Use view model
            var modelWrapper = new PropertyListWrapperViewModel
            {
                PLVMs = propList,
                Regions = regions
            };
            return View(modelWrapper);
        }

        public ActionResult Region(string id) // (regionId/marketId)
        {
            // make sure to replace the hyphens in the name
            //int marketId = db.Markets.Where(x => x.Name == id.Replace("-", " ")).FirstOrDefault().MarketID;
            var marketId = _marketRepository
                .GetMarkets()
                .FirstOrDefault(x => x.Name == id.Replace("-", " "))
                .MarketId;
            var tempList = _propertyRepository.GetProperties()
                .Where(x => x.SiteVisibilityFlags.HasFlag(Enums.SiteVisibilityFlags.Corporate)
                    || (x.SiteVisibilityFlagsOverride 
                    && x.SiteVisibilityFlagsOverrideValue.HasFlag(Enums.SiteVisibilityFlags.Corporate))) // just ones visible to 'Corporate'
                .ToList();
            var propList = tempList
                .Where(x => x.MarketId == marketId)
                .OrderBy(x => x.Name)
                .Select(y => new PropertyListViewModel { Name = y.Name, City = y.City })
                .ToList();
            var regions = _marketRepository.GetMarkets()
                .OrderBy(x => x.SortOrder)
                .ToList();
            // Use view model
            var modelWrapper = new PropertyListWrapperViewModel
            {
                RouteIdValue = id,
                PLVMs = propList,
                Regions = regions
            };
            return View("Index", modelWrapper);
        }

        // GET: Property/Details/5
        public ActionResult Details(string id)
        {
            // if no id, redirect back to index
            if (string.IsNullOrEmpty(id)) return RedirectToAction("Index");

            // breakdown id string
            var propertyLinkText = id.Split(new string[] { "-in-" }, StringSplitOptions.None);
            var name = propertyLinkText[0].Replace("-", " ");
            var city = propertyLinkText[1].Replace("-", " ");
            var property = _propertyRepository
                .GetProperties()
                .FirstOrDefault(x => x.Name == name && x.City == city);
            if (property == null)
            {
                property = _propertyRepository
                    .GetProperties()
                    .FirstOrDefault(x => x.NameOverrideValue == name && x.City == city);
                if (property == null)
                    return HttpNotFound();
            }

            // Use View Model to display Details
            var pDvm = MapOneDomainModelWithOverridesToOneViewModel(property);

            // SEO
            ViewBag.Title = property.NameOverride 
                ? property.NameOverrideValue + " in " + property.City 
                : property.Name + " in " + property.City;
            ViewBag.Description = property.DescriptionOverride
                ? property.DescriptionOverrideValue
                : property.Description;
            ViewBag.Keywords = ContentSpecific.GetCommaDelimNameStringFromTags(property.Tags.ToList());
 
            return View(pDvm);
        }

        private static List<PropertyDetailsViewModel> DomainModelsToViewModels(IEnumerable<Property> propList)
        {
            return propList.Select(MapOneDomainModelWithOverridesToOneViewModel).ToList();
            //var pDVMs = new List<PropertyDetailsViewModel>();
            //foreach (var property in propList)
            //{
            //    var model = MapOneDomainModelWithOverridesToOneViewModel(property);
            //    pDVMs.Add(model);
            //}
            //return pDVMs;
        }

        private static PropertyDetailsViewModel MapOneDomainModelWithOverridesToOneViewModel(Property property)
        {
            var pDvm = new PropertyDetailsViewModel
            {
                PropertyId = property.PropertyId,
                Address = property.Address,
                Address2 = property.Address2,
                City = property.City,
                Market = property.Market,
                State = property.State,
                Zip = property.Zip,
                Name = property.NameOverride ? property.NameOverrideValue : property.Name,
                Description = property.DescriptionOverride ? property.DescriptionOverrideValue : property.Description,
                GrossLeasableArea =
                    property.GrossLeasableAreaOverride
                        ? property.GrossLeasableAreaOverrideValue
                        : property.GrossLeasableArea,
                MaxContiguous =
                    property.MaxContiguousOverride ? property.MaxContiguousOverrideValue : property.MaxContiguous,
                MinDivisible =
                    property.MinDivisibleOverride ? property.MinDivisibleOverrideValue : property.MinDivisible,
                SquareFootage =
                    property.SquareFootageOverride ? property.SquareFootageOverrideValue : property.SquareFootage,
                CorporateContact =
                    property.CorporateContactOverride ? property.CorporateContactOverrideValue : property.CorporateContact,
                LeasingAgent = 
                    property.LeasingAgentOverride ? property.LeasingAgentOverrideValue : property.LeasingAgent,
                PropertyContent =
                    property.PropertyContentOverride ? property.PropertyContentOverrideValue : property.PropertyContent,
                SiteVisibilityFlags =
                    property.SiteVisibilityFlagsOverride
                        ? property.SiteVisibilityFlagsOverrideValue
                        : property.SiteVisibilityFlags,
                Location = property.LocationOverride ? property.LocationOverrideValue : property.Location,
                LocationDescription = property.LocationDescriptionOverride ? property.DescriptionOverrideValue ?? string.Empty : property.LocationDescription ?? string.Empty,
                Tags = property.TagsOverride
                    ? property.TagsOverrideValue.GroupBy(x => x.TagName)
                        .Select(y => y.First())
                        .ToList()
                    : property.Tags.GroupBy(x => x.TagName)
                        .Select(y => y.First())
                        .ToList(),
                WebsiteUrl = property.WebsiteUrl,
                FreeFormFields = property.FreeFormFields
            };
            var bayOptionsWithOverrides = MapBayOptionsDomainModelsToViewModels(property);
            pDvm.BayOptions = bayOptionsWithOverrides;

            return pDvm;
        }

        private static List<BayOption> MapBayOptionsDomainModelsToViewModels(Property property)
        {
            return (from bo
                        in property.BayOptions
                    where bo.SiteVisibilityFlags.HasFlag(Enums.SiteVisibilityFlags.Corporate)
                    || (bo.SiteVisibilityFlagsOverride
                    && bo.SiteVisibilityFlagsOverrideValue.HasFlag(Enums.SiteVisibilityFlags.Corporate))
                    select MapOneBayOptionDomainModelToOneViewModel(bo))
                    .ToList();
        }

        private static BayOption MapOneBayOptionDomainModelToOneViewModel(BayOption bo)
        {
            var boReturn = new BayOption
            {
                BayNumber = bo.BayNumberOverride ? bo.BayNumberOverrideValue : bo.BayNumber,
                BaySize = bo.BaySizeOverride ? bo.BaySizeOverrideValue : bo.BaySize,
                BayType = bo.BayTypeOverride ? bo.BayTypeOverrideValue : bo.BayType,
                Description = bo.DescriptionOverride ? bo.DescriptionOverrideValue : bo.Description,
                Excerpt = bo.ExcerptOverride ? bo.ExcerptOverrideValue : bo.Excerpt,
                IsFeatured = bo.IsFeaturedOverride ? bo.IsFeaturedOverrideValue : bo.IsFeatured,
                OccupancyState = bo.OccupancyStateOverride ? bo.OccupancyStateOverrideValue : bo.OccupancyState,
                SiteVisibilityFlags =
                    bo.SiteVisibilityFlagsOverride ? bo.SiteVisibilityFlagsOverrideValue : bo.SiteVisibilityFlags,
                FreeFormFields = bo.FreeFormFields,
                BayGUID = bo.BayGUID,
                ProjectGUID = bo.ProjectGUID
            };
            return boReturn;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _propertyRepository.Dispose();
                _marketRepository.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}
