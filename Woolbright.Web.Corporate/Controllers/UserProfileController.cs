﻿using System;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Corporate.Controllers
{
    public class UserProfileController : Controller
    {
        private readonly IUserProfileRepository _userRepository;
        
        public UserProfileController()
        {
            var context = new CMSContext();
            this._userRepository = new UserProfileRepository(context);
        }
        public UserProfileController(IUserProfileRepository userRepository)
        {
            this._userRepository = userRepository;
        }   
  
        // GET: Contact
        //public ActionResult Index()
        //{
        //    return View(userRepository.GetUsers()
        //                                .ToList());
        //}

        // GET: Contact/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var contact = _userRepository.GetUserProfileById(idInt);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        //// GET: Contact/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Contact/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,FirstName,LastName,Phone,Email")] UserProfile contact)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _userRepository.InsertUserProfile(contact);
        //        _userRepository.Save();
        //        return RedirectToAction("Index");
        //    }
        //    return View(contact);
        //}

        // GET: Contact/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var idInt = Convert.ToInt32(id);
        //    var contact = _userRepository.GetUserProfileById(idInt);
        //    if (contact == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contact);
        //}

        //// POST: Contact/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Phone,Email")] UserProfile contact)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _userRepository.UpdateUserProfile(contact);//.State = EntityState.Modified;
        //        //db.Entry(contact).State = EntityState.Modified;
        //        _userRepository.Save();
        //        return RedirectToAction("Index");
        //    }
        //    return View(contact);
        //}

        //// GET: Contact/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var idInt = Convert.ToInt32(id);
        //    var contact = _userRepository.GetUserProfileById(idInt);
        //    if (contact == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(contact);
        //}

        //// POST: Contact/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    _userRepository.DeleteUserProfile(id);
        //    _userRepository.Save();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
