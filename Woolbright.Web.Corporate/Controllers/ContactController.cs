﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Configuration;
using System.Web.Mvc;
using Woolbright.Web.Corporate.ViewModels;
//using SendGrid;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Corporate.Controllers
{
    public class ContactController : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
    
        public ContactController(
            IPropertyRepository propertyRepository 
        )
        {
            _propertyRepository = propertyRepository;
        }

        // GET: Contact
        public ActionResult Index(string id)
        {
            ViewBag.PropertyId = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(SendEmailViewModel emailForm)
        {
            int propertyId = 0;
            try
            {
                var mailMsg = new MailMessage();
                
                // if it's from a property, get the contact. If no contact, use default from web.config
                if (int.TryParse(emailForm.PropertyId, out propertyId))
                {
                    var property = _propertyRepository.GetPropertyById(propertyId);
                    if (property.CorporateContactOverride && property.CorporateContactOverrideValue != null)
                    {
                        // To
                        mailMsg.To.Add(new MailAddress(property.CorporateContactOverrideValue.EmailAddress,
                            property.CorporateContactOverrideValue.FullName));
                        // Bcc default just in case
                        mailMsg.Bcc.Add(new MailAddress(WebConfigurationManager.AppSettings["SendEmailToAddress"],
                            WebConfigurationManager.AppSettings["SendEmailToName"]));
                    }
                    else if (!property.CorporateContactOverride && property.CorporateContact != null)
                    {
                        // To
                        mailMsg.To.Add(new MailAddress(property.CorporateContact.EmailAddress,
                            property.CorporateContact.FullName));
                        // Bcc default just in case
                        mailMsg.Bcc.Add(new MailAddress(WebConfigurationManager.AppSettings["SendEmailToAddress"],
                            WebConfigurationManager.AppSettings["SendEmailToName"]));
                    }
                    else
                    {
                        // To
                        mailMsg.To.Add(new MailAddress(WebConfigurationManager.AppSettings["SendEmailToAddress"],
                            WebConfigurationManager.AppSettings["SendEmailToName"]));
                    }
                }
                else
                {
                    // To
                    mailMsg.To.Add(new MailAddress(WebConfigurationManager.AppSettings["SendEmailToAddress"],
                        WebConfigurationManager.AppSettings["SendEmailToName"]));
                }
               
                // From
                mailMsg.From = new MailAddress(emailForm.Email, emailForm.Name);

                // Subject and multipart/alternative Body
                mailMsg.Subject = "Contact Form: Woolbright Corporate";
                var text = @"Name: " + emailForm.Name + " Message: " + emailForm.Message;
                var html = @"Name: " + emailForm.Name + " Message:<p>" + emailForm.Message + "</p>";
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null,
                    MediaTypeNames.Text.Plain));
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null,
                    MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                var smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
                var credentials =
                    new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["SendGridAPIId"],
                        WebConfigurationManager.AppSettings["SendGridAPIKey"]);
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = true;// NEEDED: wasn't in their documents!!

                smtpClient.Send(mailMsg);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
                ViewBag.PropertyId = propertyId;
                ViewBag.ErrorMessage =
                    "There was an error processing your request. Our Administrator has been informed. Please try again later.";
                // TODO: send an alert somehow that this failed. Fallback plan?
                return View("Index");
            }
            ViewBag.SuccessMessage =
                     "Your request has been sent!";

            return View("EmailSent");
        }

	}
}
