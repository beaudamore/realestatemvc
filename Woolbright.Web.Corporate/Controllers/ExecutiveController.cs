﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Corporate.Controllers
{
    public class ExecutiveController : Controller
    {
        private readonly IExecutiveRepository _executiveRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly IExecutiveSectionRepository _executiveSectionRepository;

        public ExecutiveController(
            IExecutiveRepository executiveRepository, 
            IUserProfileRepository userRepository, 
            IExecutiveSectionRepository executiveSectionRepository)
        {
            _executiveRepository = executiveRepository;
            _userRepository = userRepository;
            _executiveSectionRepository = executiveSectionRepository;
        }

        //[HttpGet]
        public ActionResult ExecutiveProfile(string id)
        {
            // show section in id
            var executiveSectionMappings = _executiveSectionRepository.GetExecutiveSections();

            //var execs = _executiveRepository.GetExecutives()
            //                                .ToList()
            //                                .Where(x => x.ExecutiveSectionMappings.Select(c => string.Equals(c.ExecutiveSection.SectionName, id.Replace("-", " "), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault())
            //                                .OrderBy(x => x.ExecutiveSectionMappings.Select(c => c.SortOrder).FirstOrDefault());
            //if (!string.IsNullOrEmpty(id))
            //{
            //    ViewBag.Title = id.Replace("-", " ").ToUpper();
            //    var firstOrDefault = _executiveSectionRepository
            //        .GetExecutiveSections()
            //        .FirstOrDefault(x => x.SectionName == id.Replace("-", " "));
            //    if (firstOrDefault != null)
            //        ViewBag.Blurb = firstOrDefault
            //            .SectionBlurb;
            //}
            return View(executiveSectionMappings);
        }
        
        // method to handle sections
        [HttpGet]
        public ActionResult Section(string id)
        {
            // show section in id
            var execs = _executiveRepository.GetExecutives()
                                            .ToList()
                                            .Where(x => x.ExecutiveSectionMappings.Select(c => string.Equals(c.ExecutiveSection.SectionName, id.Replace("-"," "), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault())
                                            .OrderBy(x => x.ExecutiveSectionMappings.Select(c => c.SortOrder).FirstOrDefault());
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Title = id.Replace("-", " ").ToUpper();
                var firstOrDefault = _executiveSectionRepository
                    .GetExecutiveSections()
                    .FirstOrDefault(x => x.SectionName == id.Replace("-", " "));
                if (firstOrDefault != null)
                    ViewBag.Blurb = firstOrDefault
                        .SectionBlurb;
            }
            return View(execs);
        }

        // GET: Executive/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var idInt = Convert.ToInt32(id);
            var executive = _executiveRepository.GetExecutiveById(idInt);
            if (executive == null)
            {
                return HttpNotFound();
            }
            // TODO: map excutive to ViewModel


            return View(executive);
        }

        // single method for all executive pages
        //[HttpGet]
        public ActionResult ExecutiveSubMenu()
        {
            // get sections
            var sections = _executiveSectionRepository.GetExecutiveSections()
                .OrderBy(x => x.SortOrder)
                .ToList();
            return PartialView("_ExecutiveSubMenuView", sections);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _executiveRepository.Dispose();
                _userRepository.Dispose();
                _executiveSectionRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
