﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Woolbright.Web.Corporate.ViewModels;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Corporate.Controllers
{
    public class SearchController : Controller
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IResearchArticleRepository _researchArticleRepository;
        public SearchController(
            IPropertyRepository propertyRepository, 
            IResearchArticleRepository researchArticleRepository
            )
        {
            _propertyRepository = propertyRepository;
            _researchArticleRepository = researchArticleRepository;
        }

        // GET: Search
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string searchString)
        {
            var wrapperModel = new SearchResultsWrapperViewModel();
            if (string.IsNullOrEmpty(searchString)) return View(wrapperModel);
            var stringResults = _propertyRepository
                .GetProperties()
                .Where(
                    // standard fields
                    x => x.Address.IfNullOrWhiteSpace("").Contains(searchString)
                         || x.City.IfNullOrWhiteSpace("").Contains(searchString)
                         || x.WebsiteUrl.IfNullOrWhiteSpace("").Contains(searchString)
                         || x.Zip.IfNullOrWhiteSpace("").Contains(searchString)
                        // overrides possible
                         || (x.DescriptionOverride ? x.DescriptionOverrideValue.Contains(searchString) : x.Description.IfNullOrWhiteSpace("").Contains(searchString))
                         //|| (x.GrossLeasableAreaOverride ? x.GrossLeasableAreaOverrideValue.ToString().Contains(searchString) : x.GrossLeasableArea.ToString().Contains(searchString))
                         //|| (x.MaxContiguousOverride ? x.MaxContiguousOverrideValue.ToString().Contains(searchString) : x.MaxContiguous.ToString().Contains(searchString))
                         //|| (x.MinDivisibleOverride ? x.MinDivisibleOverrideValue.ToString().Contains(searchString) : x.MinDivisible.ToString().Contains(searchString))
                         || (x.NameOverride ? x.NameOverrideValue.Contains(searchString) : x.Name.Contains(searchString))
                         || (x.SquareFootageOverride ? x.SquareFootageOverrideValue.ToString().Contains(searchString) : x.SquareFootage.ToString().Contains(searchString))
                        // tags
                         || (x.TagsOverride ? x.TagsOverrideValue.Any(f => f.TagName.Contains(searchString)) : x.Tags.Any(f => f.TagName.Contains(searchString)))
                        // bayoptions
                         || x.BayOptions.Any(g => g.BaySizeOverride ? g.BaySizeOverrideValue.ToString().Contains(searchString) : g.BaySize.ToString().Contains(searchString))
                         //|| x.BayOptions.Any(g => g.BayTypeOverride ? g.BayTypeOverrideValue.Name.Contains(searchString) : g.BayType.Name.Contains(searchString))
                         || x.BayOptions.Any(g => g.DescriptionOverride ? g.DescriptionOverrideValue.IfNullOrWhiteSpace("").Contains(searchString) : g.Description.IfNullOrWhiteSpace("").Contains(searchString))
                         || x.BayOptions.Any(g => g.ExcerptOverride ? g.ExcerptOverrideValue.IfNullOrWhiteSpace("").Contains(searchString) : g.Excerpt.IfNullOrWhiteSpace("").ToString().Contains(searchString))
                        // Freeform Fields
                         || x.FreeFormFields.Any(g => g.NumberValue.ToString().Contains(searchString))
                         || x.FreeFormFields.Any(g => g.StringValue != null && g.StringValue.ToString().Contains(searchString))
                ).ToList();

            var pdvms = new List<Property>();
            foreach (var item in stringResults)
            {
                var pdvm = new Property
                {
                    Address = item.Address,
                    Address2 = item.Address2,
                    BayOptions = item.BayOptions,
                    City = item.City,
                    FreeFormFields = item.FreeFormFields,
                    PropertyId = item.PropertyId,
                    State = item.State,
                    Tags = item.Tags,
                    WebsiteUrl = item.WebsiteUrl,
                    Zip = item.Zip
                };
                pdvm.Description = item.DescriptionOverride ? pdvm.Description = item.DescriptionOverrideValue : pdvm.Description = item.Description;
                pdvm.GrossLeasableArea = item.GrossLeasableAreaOverride ? item.GrossLeasableAreaOverrideValue : item.GrossLeasableArea;
                pdvm.Location = item.LocationOverride ? item.LocationOverrideValue : item.Location;
                pdvm.MaxContiguous = item.MaxContiguousOverride ? item.MaxContiguousOverrideValue : item.MaxContiguous;
                pdvm.MinDivisible = item.MinDivisibleOverride ? item.MinDivisibleOverrideValue : item.MinDivisible;
                pdvm.Name = item.NameOverride ? item.NameOverrideValue : item.Name;
                pdvm.CorporateContact = item.CorporateContactOverride ? item.CorporateContactOverrideValue : item.CorporateContact;
                pdvm.PropertyContent = item.PropertyContentOverride ? item.PropertyContentOverrideValue : item.PropertyContent;
                pdvm.SquareFootage = item.SquareFootageOverride ? item.SquareFootageOverrideValue : item.SquareFootage;
                pdvms.Add(pdvm);
            }
            wrapperModel.Properties = pdvms;
                
            // research articles
            var racvm = _researchArticleRepository
                .GetResearchArticles()
                .Where(x => x.BodyAzureContainerName.Contains(searchString)
                            || x.Title.Contains(searchString)
                            || x.Tags.Select(m => m.TagName).Contains(searchString))
                .ToList();
            //List
            wrapperModel.ResearchArticles = racvm;

            return View(wrapperModel);
        }
       
    }
}
