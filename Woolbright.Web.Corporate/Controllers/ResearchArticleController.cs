﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Woolbright.Web.Corporate.ViewModels;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Corporate.Controllers
{
    public class ResearchArticleController : Controller
    {
        private readonly IResearchArticleRepository _researchArticleRepository;
        private readonly IContentRepository _contentRepository;
      
        public ResearchArticleController(
            IResearchArticleRepository researchArticleRepository, 
            IContentRepository contentRepository
            )
        {
            _researchArticleRepository = researchArticleRepository;
            _contentRepository = contentRepository;
        }

        // GET: ResearchArticle
        [HttpGet]
        public ActionResult Index(string id) // if 'id' is passed it's coming from the Search Results
        {
            // viewmodel
            var viewModel = new ResearchArticleViewModel
            {
                ArchiveList = GetArchiveList()
            };
            // for archive DDL - changed from date/count to date/date where one has 'july' instead of 7
            ViewBag.ArchiveList = new SelectList(viewModel.ArchiveList, "Key", "Value", null);
            
            // get archives or specific article
            if (!string.IsNullOrEmpty(id))
            {
                var newId = Convert.ToInt32(id);
                viewModel.Articles = _researchArticleRepository
                    .GetResearchArticles()
                    .Where(x => x.ArticleId == newId)
                    .ToList();
                // meta tags
                var article = viewModel.Articles
                    .FirstOrDefault();
                if (article != null)
                {
                    ViewBag.Title = article.Title;
                    ViewBag.Description = article.ShortDescription;
                    ViewBag.Keywords = article.Tags;
                }
            }
            else
            {
                // current monthly articles
                viewModel.Articles = _researchArticleRepository.GetResearchArticles()
                    .Where(
                        x =>
                            x.DatePublished.Year == DateTime.Today.Year 
                            && x.DatePublished.Month == DateTime.Today.Month)
                    .ToList(); // this month only     
            }
           
           return View(viewModel);
        }

        public ActionResult Archive(string id)// this is in format 'mm-yyyy'
        {
            // viewmodel to return
            var viewModel = new ResearchArticleViewModel();
            // parse date sent
            var postedDateInfo = id.Split('-');
            var month = int.Parse(postedDateInfo[0]);
            var year = int.Parse(postedDateInfo[1]);
            // get articles
            viewModel.Articles = _researchArticleRepository.GetResearchArticles()
                .Where(x => x.DatePublished.Month == month && x.DatePublished.Year == year)
                .ToList();          
            // get archives
            viewModel.ArchiveList = GetArchiveList();
            // for archive ddl
            ViewBag.ArchiveListBag = new SelectList(viewModel.ArchiveList, "Key", "Value", id);
           
            return View("Index", viewModel);
        }

        private Dictionary<string, string> GetArchiveList()
        {
            //var lastDayOfLastMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);
            var grouped = (from p in _researchArticleRepository.GetResearchArticles()
                           group p by new { month = p.DatePublished.Month, year = p.DatePublished.Year } into d
                           select new { dt = d.Key.month + "-" + d.Key.year, 
                                        dtByMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(d.Key.month) + " " +  d.Key.year}//count = d.Count() }
                           )
                           .OrderByDescending(g => g.dt);
            return grouped.ToDictionary(item => item.dt, item => item.dtByMonthName);
            // pre-resharper
            //foreach (var item in grouped)
            //{
            //    archiveList.Add(item.dt, item.count);
            //}
            //return archiveList;
        }


        // GET: ResearchArticle/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // parse title-on-date sent
            var articleLinkText = id.Split(new string[] { "-on-" }, StringSplitOptions.None);
            var title = articleLinkText[0].Replace("-", " ");
            var datePublished = DateTime.Parse(articleLinkText[1]);

            var researchArticle = _researchArticleRepository
                .GetResearchArticles()
                .FirstOrDefault(x => x.Title == title && x.DatePublished == datePublished);
            if (researchArticle == null)
            {
                return HttpNotFound();
            }
            // SEO
            ViewBag.Title = researchArticle.Title;
            ViewBag.Description = researchArticle.ShortDescription;
            ViewBag.Keywords = ContentSpecific.GetCommaDelimNameStringFromTags(researchArticle.Tags.ToList());

            return View(researchArticle);
        }

        [HttpGet]
        public FileResult RetrieveFile(Guid id)
        {
            var content = _contentRepository
                            .GetContent()
                            .First(x => x.ContentGuid == id);
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + content.FileName + "");
            return File(content.ContentBytes, content.MimeType);
        }

        [HttpGet]
        public ActionResult GetContentByArticleId(int id)
        {
            var article = _researchArticleRepository
                .GetResearchArticles()
                .FirstOrDefault(x => x.ArticleId == id);
            return PartialView("_ResearchArticleContentView", article.Thumbnail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _researchArticleRepository.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
