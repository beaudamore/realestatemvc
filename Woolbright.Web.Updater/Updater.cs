﻿using System;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Linq;
using Woolbright.Web.Data.Classes;
using Woolbright.Web.Data.DAL;
using Woolbright.Web.Data.Models;

namespace Woolbright.Web.Updater
{
    class Updater
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IBayOptionRepository _bayOptionRepository;
        private readonly IBayTypeRepository _bayTypeRepository;
        private readonly IMarketRepository _marketRepository;
        private readonly IUserProfileRepository _userRepository;
        private readonly PETSEntities _pets_dc;
        private readonly POEMSEntities _poems_dc;

        public Updater(
            IPropertyRepository propertyRepository,
            IBayOptionRepository bayOptionRepository,
            IBayTypeRepository bayTypeRepository,
            IMarketRepository marketRepository,
            IUserProfileRepository userRepository,
            PETSEntities pets_dc,
            POEMSEntities poems_dc
            )
        {
            _propertyRepository = propertyRepository;
            _bayOptionRepository = bayOptionRepository;
            _bayTypeRepository = bayTypeRepository;
            _marketRepository = marketRepository;
            _userRepository = userRepository;
            _pets_dc = pets_dc;
            _poems_dc = poems_dc;
        }

        public void UpdateMarkets(SqlDateTime? sinceDate)
        {
            if (!_marketRepository.GetMarkets().Any())
            {
                var market = new Market
                {
                    Name = "Test",
                    SortOrder = 1
                };
                _marketRepository.InsertMarket(market);
                _marketRepository.Save();
            }
        }

        public void UpdateUsers(SqlDateTime? sinceDate)
        {
            // get list of existing Users to join to Contacts
            var users = _pets_dc.Users
                .Select(p => p)
                .Where(x => x.Active) // commenting this out gives 352 users total but still not the property contact(s)
                .ToList();

            var contacts = _poems_dc.Contacts
                .Select(p => p)
                .Where(x => x.Active)
                .ToList();

            var query = from Users in users
                        join Contacts in contacts
                        on Users.Contact_GUID equals Contacts.Contact_GUID
                        select new { Contacts };
          
            if (sinceDate.HasValue)
                query = query
                    .Where(u => u.Contacts.ModificationDate >= (DateTime)sinceDate)
                    .ToList();
            
            // compare to existing via get/modify or create
            var counter = 1;
            foreach (var contact in query)
            {
                var u = _userRepository.GetUserProfileByGuid(contact.Contacts.Contact_GUID);
                if (u != null) // update
                {
                    u.FirstName = contact.Contacts.FirstName;
                    u.LastName = contact.Contacts.LastName;
                    u.Address1 = contact.Contacts.BusinessAddress1 ?? string.Empty;
                    u.Address2 = contact.Contacts.BusinessAddress2 ?? string.Empty;
                    u.City = contact.Contacts.BusinessCity ?? string.Empty;
                    var firstOrDefault = _pets_dc.States.FirstOrDefault(x => x.State_GUID == contact.Contacts.BusinessState_GUID);
                    if (firstOrDefault != null)
                        u.State = firstOrDefault.StateAbbreviation;
                    u.Zip = contact.Contacts.BusinessZipCode ?? string.Empty;
                    u.EmailAddress = contact.Contacts.EMailAddress ?? string.Empty;
                    u.WorkPhoneNumber = contact.Contacts.BusinessPhoneNumber.HasValue ? contact.Contacts.BusinessPhoneNumber.ToString() : string.Empty;
                    u.CellPhoneNumber = contact.Contacts.CellPhoneNumber.HasValue ? contact.Contacts.CellPhoneNumber.ToString() : string.Empty;
                    u.FaxNumber = contact.Contacts.FaxNumber.HasValue ? contact.Contacts.FaxNumber.ToString() : string.Empty;
                    u.IsActive = contact.Contacts.Active;

                    _userRepository.UpdateUserProfile(u);
                }
                else // new
                {
                    var newUser = new UserProfile();
                    newUser.UserProfileGuid = contact.Contacts.Contact_GUID;
                    newUser.FirstName = contact.Contacts.FirstName;
                    newUser.LastName = contact.Contacts.LastName;
                    newUser.Address1 = contact.Contacts.BusinessAddress1 ?? string.Empty;
                    newUser.Address2 = contact.Contacts.BusinessAddress2 ?? string.Empty;
                    newUser.City = contact.Contacts.BusinessCity ?? string.Empty;
                    // state
                    if (contact.Contacts.BusinessState_GUID != new Guid() 
                        && contact.Contacts.BusinessState_GUID != null) // some BusinessState_GUID's are blank rather than null; ie:0000-00-000...
                    {
                        newUser.State = contact.Contacts.BusinessState_GUID.HasValue
                            ? _pets_dc.States.FirstOrDefault(x => x.State_GUID == contact.Contacts.BusinessState_GUID)
                                .StateAbbreviation
                            : string.Empty;
                    }
                    newUser.Zip = contact.Contacts.BusinessZipCode ?? string.Empty;
                    newUser.EmailAddress = contact.Contacts.EMailAddress ?? string.Empty;
                    newUser.WorkPhoneNumber = contact.Contacts.BusinessPhoneNumber.HasValue
                        ? contact.Contacts.BusinessPhoneNumber.ToString()
                        : string.Empty;
                    newUser.CellPhoneNumber = contact.Contacts.CellPhoneNumber.HasValue
                        ? contact.Contacts.CellPhoneNumber.ToString()
                        : string.Empty;
                    newUser.FaxNumber = contact.Contacts.FaxNumber.HasValue
                        ? contact.Contacts.FaxNumber.ToString()
                        : string.Empty;
                    newUser.IsActive = contact.Contacts.Active;
                    
                    _userRepository.InsertUserProfile(newUser);
                }
                Console.WriteLine("Users: " + counter + " of " + query.Count() + " done: " + Convert.ToInt32(counter * 100 / query.Count()) + "%");
                _userRepository.Save();
               counter++;
            }
        }

        public void UpdateBayTypes(SqlDateTime? sinceDate)
        {
            var counter = 0;
            var query = _poems_dc.BayTypes.ToList();
            foreach (var bt in query)
            {
                // existing update
                var existingBt = _bayTypeRepository.GetBayTypes().FirstOrDefault(x => x.BayTypeGuid == bt.BayType_GUID);
                if (existingBt != null)
                {
                    existingBt.Name = bt.BayType1;
                    existingBt.Name = bt.BayType1;
                    existingBt.Active = bt.Active;
                    existingBt.HiddenValue = bt.HiddenValue;
                }
                else  // new
                {
                    var bayType = new Data.Models.BayType();
                    bayType.BayTypeGuid = bt.BayType_GUID;
                    bayType.Name = bt.BayType1;
                    bayType.Active = bt.Active;
                    bayType.HiddenValue = bt.HiddenValue;
                    _bayTypeRepository.InsertBayType(bayType);
                }
                counter++;
                Console.WriteLine("Bay Types: " + counter + " of " + query.Count() + " done: " + Convert.ToInt32(counter * 100 / query.Count()) + "%");
            }
            _bayTypeRepository.Save();
        }

        public void UpdateProperties(SqlDateTime? sinceDate)
        {
            // compare what's in web db to the master Db to delete removed properties

            // get list of existing properties from master Db
            var query = _poems_dc.Projects.Select(p => p).Where(x => !x.TestProject);

            if (sinceDate.HasValue)
                query = query.Where(u => u.ModificationDate >= (DateTime)sinceDate);

            // compare to existing via get/modify or create
            var counter = 1; // for dev feedback
            foreach (var poemsproject in _poems_dc.Projects)
            {
                var c = GetProperty(poemsproject.Project_GUID);
                if (c == null)
                {
                    // For New centers:
                    c = new Property
                    {
                        Name = poemsproject.ProjectName,
                        ProjectGuid = poemsproject.Project_GUID,
                        Market = _marketRepository.GetMarkets().FirstOrDefault(),
                        SiteVisibilityFlags = Enums.SiteVisibilityFlags.None
                    };
                    _propertyRepository.InsertProperty(c);
                }
                else
                {
                    c.SiteVisibilityFlags = poemsproject.ShowOnWebSite
                        ? Enums.SiteVisibilityFlags.Corporate // TODO: determine where it is to be shown if .ShowOnWebSite=true
                        : Enums.SiteVisibilityFlags.None;
                }

                // Always update:
                // For existing centers:
                // corporate contact
                if (poemsproject.LeasingAgent_GUID.HasValue)
                {
                    var u = _userRepository.GetUserProfileByGuid((Guid)poemsproject.LeasingAgent_GUID);
                    if (u != null)
                    {
                        c.CorporateContact = u;
                    }
                    else
                    {
                        try
                        {
                            // bring user over
                            var petsUserContactGuid = _pets_dc.Users
                                .SingleOrDefault(x => x.User_GUID == poemsproject.LeasingAgent_GUID)
                                .Contact_GUID;
                            var poemsContact =
                                _poems_dc.Contacts.SingleOrDefault(x => x.Contact_GUID == petsUserContactGuid);
                            var up = new UserProfile();
                            up.UserProfileGuid = poemsproject.LeasingAgent_GUID ?? Guid.Empty;
                            up.FirstName = poemsContact.FirstName;
                            up.LastName = poemsContact.LastName;
                            up.WorkPhoneNumber = poemsContact.BusinessPhoneNumber.ToString();
                            up.EmailAddress = poemsContact.EMailAddress;
                            up.CellPhoneNumber = poemsContact.CellPhoneNumber.ToString();
                            _userRepository.InsertUserProfile(up);
                            _userRepository.Save();
                            c.CorporateContact = up;
                  
                        }
                        catch (Exception ex)
                        {
                            // flow through but write this error down somewhere...
                        }
                        
                        //var userFromPets = _pets_dc.Users.Where(x => x.Contact_GUID == poemsproject.LeasingAgent_GUID.Value);
                        // TODO: map user from pets to UserProfile
                    }
                }
               
                // description
                c.Description = poemsproject.Description;
               
                // TODO: do 'Properties" have addresses or just 'BayOptions'?
                var petsproject = _pets_dc.PetsProjects
                    .SingleOrDefault(pts => pts.Project_GUID == poemsproject.Project_GUID);
                //petsproject.Location
                if (petsproject != null)
                {
                    if (petsproject.Latitude != null && petsproject.Longitude != null)
                    c.Location = DbGeography.FromText("POINT(" + petsproject.Longitude + " " + petsproject.Latitude + ")");
                    //c.Location = ContentSpecific.FindCoordinatesFromGoogleMaps(petsproject.Location);
                    c.City = petsproject.City;
                    c.WebsiteUrl = petsproject.LeasingWebSite;
                    c.LocationDescription = petsproject.Location;
                }

                // bay options
                var totalGlaForProperty = 0;
                var counterBO = 1;
                foreach (var bay in poemsproject.Bays)
                {
                    // see if bay exists
                    BayOption existingBay = null;
                    if (c.BayOptions != null)
                    {
                        existingBay = c.BayOptions.FirstOrDefault(x => x.BayGUID == bay.Bay_GUID);
                    }
                    // update or create
                    if (existingBay != null)    // update
                    {
                        existingBay.ProjectGUID = bay.Project_GUID;
                        existingBay.BaySize = bay.BaySize;
                        // get baytype
                        var bayType = _bayTypeRepository.GetBayTypes().FirstOrDefault(x => x.BayTypeGuid == bay.BayType_GUID);
                        if (bayType != null)
                        {
                            existingBay.BayType = bayType;
                        }
                        if (!bay.Active) // if not Active, set to None
                            existingBay.SiteVisibilityFlags = Enums.SiteVisibilityFlags.None;
                        existingBay.Address = bay.BayAddress;

                        // if Inactive before and now Active
                        if (!existingBay.IsActive && bay.Active)
                            existingBay.UpdatedSinceImport = false;
                        existingBay.IsActive = bay.Active;
                        
                    }
                    else              // create
                    {
                        var bo = new BayOption();
                        bo.BayGUID = bay.Bay_GUID;
                        bo.ProjectGUID = bay.Project_GUID;
                        bo.IsActive = bay.Active;
                        bo.BaySize = bay.BaySize;
                        // get baytype
                        var bayType = _bayTypeRepository.GetBayTypes().FirstOrDefault(x => x.BayTypeGuid == bay.BayType_GUID);
                        if (bayType != null)
                        {
                            bo.BayType = bayType;
                        }
                        bo.SiteVisibilityFlags = Enums.SiteVisibilityFlags.None;
                        bo.Address = bay.BayAddress;
                        bo.Property = c;
                        bo.UpdatedSinceImport = false;
                        _bayOptionRepository.InsertBayOption(bo);
                    }
                    // sum total for property totalGla
                    totalGlaForProperty += bay.BaySize;
                    
                    // dev feedback
                    Console.WriteLine("BayOptions: " + counterBO + " of " + poemsproject.Bays.Count() + " done: " + Convert.ToInt32(counterBO * 100 / poemsproject.Bays.Count()) + "%");
                    counterBO++;

                    _bayOptionRepository.Save();
                }
                c.TotalGla = totalGlaForProperty;

                // dev feedback
                Console.WriteLine();
                Console.WriteLine("Properties: " + counter + " of " + query.Count() + " done: " + Convert.ToInt32(counter * 100 / query.Count()) + "%");
                Console.WriteLine();
                Console.WriteLine();
                counter++;

                _propertyRepository.Save();
            }

            // save
            _propertyRepository.Save();
            _bayOptionRepository.Save();
        }


        #region Helpers
        private Property GetProperty(Guid projectGuid)
        {
            var query = from p in _propertyRepository.GetProperties()
                        where p.ProjectGuid == projectGuid
                        select p;
            return query.SingleOrDefault();
        }

        //private Center GetCenter(Guid projectGuid)
        //{
        //    var query = from p in dc.Centers
        //                where p.Project_GUID == projectGuid
        //                select p;

        //    return query.SingleOrDefault();
        //}
        #endregion
    }
}
