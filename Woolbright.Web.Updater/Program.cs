﻿using System.Data.SqlTypes;
using Woolbright.Web.Data.DAL;

namespace Woolbright.Web.Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            // context
            var context = new CMSContext();

            // repositories
            IPropertyRepository propertyRepository = new PropertyRepository(context);
            IBayOptionRepository bayOptionRepository = new BayOptionRepository(context);
            IBayTypeRepository bayTypeRepository = new BayTypeRepository(context);
            IMarketRepository marketRepository = new MarketRepository(context);
            IUserProfileRepository userRepository = new UserProfileRepository(context);

            // external entities
            var petsDc = new PETSEntities();
            var poemsDc = new POEMSEntities();

            // setup main class obj
            var u = new Updater(propertyRepository, bayOptionRepository, bayTypeRepository, marketRepository, userRepository, petsDc, poemsDc);

            // dateTime to check from; TODO: set this to current or previous date/time for 'live'; might need a table/col to store last run datetime
            var dateToCheckFrom = SqlDateTime.Null;

            // run each updater
            u.UpdateMarkets(dateToCheckFrom);
            u.UpdateUsers(dateToCheckFrom);
            u.UpdateBayTypes(dateToCheckFrom);
            u.UpdateProperties(dateToCheckFrom);

        }
    }
}
